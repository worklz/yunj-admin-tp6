<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [
            "yunj\core\Init"
        ],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        // 成员登录成功事件
        'AdminMemberLogin'=>[],
        // 成员退出登录事件
        'AdminMemberLogout'=>[],
        // 系统设置保存前事件
        'AdminSettingSaveBefore'=>[],
        // 表单构建器提交操作前事件
        'YunjFormSubmitBefore'=>[],
        // 表格构建器异步事件触发前事件
        'YunjTableEventBefore'=>[],
    ],

    'subscribe' => [
    ],
];
