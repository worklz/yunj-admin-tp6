# 云静Admin核心 TP6

[![](https://img.shields.io/badge/Author-Uncle.L-orange.svg)](https://gitee.com/worklz/yunj-admincore-tp6)
[![](https://img.shields.io/badge/version-v4.4.52-brightgreen.svg)](https://gitee.com/worklz/yunj-admincore-tp6)
[![star](https://gitee.com/worklz/yunj-admincore-tp6/badge/star.svg?theme=dark)](https://gitee.com/worklz/yunj-admincore-tp6/stargazers)
[![fork](https://gitee.com/worklz/yunj-admincore-tp6/badge/fork.svg?theme=dark)](https://gitee.com/worklz/yunj-admincore-tp6)

## 简介

**云静Admin核心 TP6**是云静Admin TP6的核心代码包

## 文档

安装、开发、更新详见：[云静Admin TP6](https://tp6admin.doc.iyunj.cn)

## 演示

PC端截图 | 移动端截图
--- | ---
![](http://tp6admin.doc.iyunj.cn/assets/imgs/admin-pc-home.png) | ![](http://tp6admin.doc.iyunj.cn/assets/imgs/admin-mobile-home.png)

* 演示地址：[http://tp6admin.iyunj.cn/admin](http://tp6admin.iyunj.cn/admin)

## QQ交流群

* `692392147`

## 参与开发

直接提交PR或者Issue即可