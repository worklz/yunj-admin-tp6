<?php

namespace yunj\command;

use think\console\Input;
use think\console\Output;
use yunj\app\admin\service\route\RouteFileService;
use yunj\app\admin\service\route\RouteRequestService;
use yunj\core\Command as BaseCommand;
use yunj\app\admin\service\route\RouteSyncSystemDataService;

class RouteFileReset extends BaseCommand {

    protected function configure() {
        $this->setName('yunj:route-file-reset')
            ->setDescription('云静路由文件重置');
    }

    protected function execute(Input $input, Output $output) {
        ini_set("display_errors", "On");//打开错误提示
        ini_set("error_reporting", E_ALL);//显示所有错误
        set_time_limit(3600);

        try {
            // 路由文件重置（内部执行了缓存清理）
            RouteFileService::reset();
            $this->success();
        } catch (\Throwable $e) {
            $this->error(exception_to_str($e));
        }
    }
}