<?php

namespace yunj\command;

use think\console\Input;
use think\console\Output;
use yunj\core\Command as BaseCommand;

class Init extends BaseCommand {

    protected function configure() {
        $this->setName('yunj:init')
            ->setDescription('云静系统初始化');
    }

    protected function execute(Input $input, Output $output) {
        ini_set("display_errors", "On");//打开错误提示
        ini_set("error_reporting", E_ALL);//显示所有错误
        try {
            (new \yunj\init\Init())->handle();
        } catch (\RuntimeException $e) {
            $this->error($e->getMessage());
        } catch (\Throwable $e) {
            $this->error($e);
        }
    }
}