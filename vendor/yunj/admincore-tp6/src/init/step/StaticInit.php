<?php

namespace yunj\init\step;

/**
 * 静态文件初始化
 * Class StaticInit
 * @package yunj\init\step
 */
class StaticInit extends Step {

    public function handle() {
        $dirPath = ds_replace(root_path() . 'public/static/yunj');
        // 是否存在
        if (is_dir($dirPath)) {
            // 是否有文件
            if (!is_empty_dir($dirPath)) {
                $res = $this->confirm("静态资源[{$dirPath}]已存在，是否更新为当前版本最新代码，确认继续执行？否 将跳过 {$this->desc} 流程");
                if (!$res) {
                    return self::RES_SKIP;
                }
                $this->info("更新中...");
            }
        }
        dir_writeable_mk($dirPath);
        dir_delete($dirPath, false, ds_replace($dirPath . '/.gitignore'));
        dir_copy(ds_replace(YUNJ_VENDOR_SRC_PATH . "public/static/yunj"), $dirPath);
        return self::RES_SUCCESS;
    }

}