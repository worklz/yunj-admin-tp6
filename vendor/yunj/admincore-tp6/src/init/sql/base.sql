DROP TABLE IF EXISTS `__PREFIX__admin_auth`;
CREATE TABLE `__PREFIX__admin_auth`
(
    `key`              varchar(64)  NOT NULL COMMENT '权限唯一标识',
    `parent`           varchar(64)  NOT NULL DEFAULT '' COMMENT '父级key',
    `name`             varchar(64)  NOT NULL DEFAULT '' COMMENT '名称',
    `full_name`        varchar(255) NOT NULL DEFAULT '' COMMENT '完整名称，父级名称拼接',
    `desc`             varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
    `type`             tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型。0常规、11侧边栏菜单、22顶部菜单',
    `icon`             varchar(64)  NOT NULL DEFAULT '' COMMENT '图标class',
    `page_open`        tinyint(4) NOT NULL DEFAULT '0' COMMENT '页面打开方式。0无、11tab子页面、22popup子页面、33new新标签页',
    `request_type`     tinyint(4) NOT NULL DEFAULT '11' COMMENT '请求类型。11请求功能，22自定义请求地址',
    `request_id`       int(11) DEFAULT NULL COMMENT '请求功能id',
    `request_url`      varchar(200) NOT NULL DEFAULT '' COMMENT '请求地址。比如菜单项访问外部地址，不存在请求功能id，则创建对应的请求地址',
    `menu_sort`        smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '菜单排序。默认0',
    `sort`             smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序。默认0',
    `is_system`        tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为系统请求。0否，1是',
    `is_demo`          tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为系统demo。0否，1是',
    `create_time`      int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time` int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`            tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    UNIQUE KEY `uk` (`key`) USING BTREE,
    UNIQUE KEY `uk_request_id` (`request_id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT='后台权限表' ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `__PREFIX__admin_member`;
CREATE TABLE `__PREFIX__admin_member`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `pid`              int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者ID',
    `username`         varchar(64) NOT NULL COMMENT '用户名',
    `password_salt`    varchar(32) NOT NULL COMMENT '密码salt',
    `password_hash`    varchar(32) NOT NULL COMMENT '密码hash',
    `name`             varchar(32) NOT NULL DEFAULT '' COMMENT '姓名',
    `last_login_ip`    varchar(64) NOT NULL DEFAULT '' COMMENT '最后一次登录IP',
    `last_login_time`  int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次登录时间',
    `create_time`      int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time` int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`            tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `uk_username` (`username`) USING BTREE,
    KEY                `idx_name` (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '成员表' ROW_FORMAT = Dynamic;
INSERT INTO `__PREFIX__admin_member`
VALUES (1, 0, '__ADMIN_MEMBER_USERNAME__', '__ADMIN_MEMBER_PASSWORD_SALT__', '__ADMIN_MEMBER_PASSWORD_HASH__', '管理员',
        '127.0.0.1', 1678455301, 1640966400, 1674986666, 11);

DROP TABLE IF EXISTS `__PREFIX__admin_member_log`;
CREATE TABLE `__PREFIX__admin_member_log`
(
    `id`               bigint(20) unsigned NOT NULL,
    `request_id`       int(10) unsigned NOT NULL COMMENT '请求功能id',
    `method`           varchar(8)   NOT NULL DEFAULT '' COMMENT '请求method',
    `member_id`        int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
    `data_id`          varchar(100) NOT NULL DEFAULT '' COMMENT '数据id',
    `ip`               varchar(64)  NOT NULL DEFAULT '' COMMENT 'ip',
    `ip_province`      varchar(64)  NOT NULL DEFAULT '' COMMENT 'ip所属省份',
    `ip_location`      varchar(64)  NOT NULL DEFAULT '' COMMENT 'ip归属地',
    `platform`         varchar(64)  NOT NULL DEFAULT '' COMMENT '系统平台',
    `platform_version` varchar(64)  NOT NULL DEFAULT '' COMMENT '系统平台版本号',
    `browser`          varchar(64)  NOT NULL DEFAULT '' COMMENT '浏览器',
    `browser_version`  varchar(64)  NOT NULL DEFAULT '' COMMENT '浏览器版本号',
    `create_time`      int(11) unsigned NOT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY                `idx_ip` (`ip`) USING BTREE,
    KEY                `idx_member_id` (`member_id`) USING BTREE,
    KEY                `idx_data_id` (`data_id`) USING BTREE,
    KEY                `idx_created_at` (`create_time`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT='后台成员日志表' ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `__PREFIX__admin_member_log_attr`;
CREATE TABLE `__PREFIX__admin_member_log_attr`
(
    `log_id`           bigint(20) unsigned NOT NULL,
    `user_agent`       text COMMENT '用户Agent',
    `query_string`     text COMMENT '访问地址query string',
    `request_params`   text COMMENT '请求参数',
    `response_content` text COMMENT '响应内容',
    `desc`             text COMMENT '描述',
    PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT='后台成员日志属性表' ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `__PREFIX__admin_member_role`;
CREATE TABLE `__PREFIX__admin_member_role`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `name`             varchar(128) NOT NULL COMMENT '名称',
    `alias`            varchar(64)  NOT NULL DEFAULT '' COMMENT '别名',
    `intro`            varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
    `auths`            text COMMENT '拥有的权限。如：["auth1","auth2","auth3"]',
    `sort`             int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
    `create_time`      int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time` int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`            tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `idx_alias` (`alias`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '成员角色表' ROW_FORMAT = Dynamic;
INSERT INTO `__PREFIX__admin_member_role`
VALUES (1, '超级管理员', 'administrator', '拥有所有权限', NULL, 0, 1640966400, 1640966400, 11);

DROP TABLE IF EXISTS `__PREFIX__admin_member_role_relation`;
CREATE TABLE `__PREFIX__admin_member_role_relation`
(
    `member_id` int(11) NOT NULL,
    `role_id`   int(11) NOT NULL,
    PRIMARY KEY (`member_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '成员角色关系表' ROW_FORMAT = Dynamic;
INSERT INTO `__PREFIX__admin_member_role_relation`
VALUES (1, 1);

DROP TABLE IF EXISTS `__PREFIX__admin_route`;
CREATE TABLE `__PREFIX__admin_route`
(
    `id`                int(10) unsigned NOT NULL AUTO_INCREMENT,
    `group_id`          int(11) NOT NULL DEFAULT '0' COMMENT '分组id',
    `name`              varchar(64)           DEFAULT NULL COMMENT '别名，用于TP生成地址',
    `desc`              varchar(64)  NOT NULL DEFAULT '' COMMENT '描述',
    `full_desc`         varchar(255) NOT NULL DEFAULT '' COMMENT '完整描述，父级分组描述拼接',
    `base_url`          varchar(255) NOT NULL DEFAULT '' COMMENT '基础地址',
    `rule`              varchar(128) NOT NULL COMMENT '规则',
    `route`             varchar(128) NOT NULL COMMENT '路由地址',
    `middleware`        varchar(255) NOT NULL DEFAULT '' COMMENT '中间件',
    `enable_login_auth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '登录鉴权。0未配置，1启用，2关闭',
    `sort`              int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `system_key`        varchar(64)           DEFAULT NULL COMMENT '系统路由唯一标识',
    `is_demo`           tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为系统demo。0否，1是',
    `create_time`       int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time`  int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`             tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `uk_system_key` (`system_key`) USING BTREE,
    UNIQUE KEY `uk_name` (`name`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT='后台路由表' ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `__PREFIX__admin_route_group`;
CREATE TABLE `__PREFIX__admin_route_group`
(
    `id`                int(11) unsigned NOT NULL AUTO_INCREMENT,
    `pid`               int(11) NOT NULL DEFAULT '0',
    `name`              varchar(64)  NOT NULL COMMENT '名称',
    `full_name`         varchar(255) NOT NULL DEFAULT '' COMMENT '完整名称，父级名称拼接',
    `namespace`         varchar(64)  NOT NULL DEFAULT '' COMMENT '命名空间',
    `middleware`        varchar(255) NOT NULL DEFAULT '' COMMENT '中间件',
    `enable_login_auth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '登录鉴权。0未配置，1启用，2关闭',
    `desc`              varchar(64)  NOT NULL DEFAULT '' COMMENT '描述',
    `full_desc`         varchar(255) NOT NULL DEFAULT '' COMMENT '完整描述，父级描述拼接',
    `sort`              int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `system_key`        varchar(64)           DEFAULT NULL COMMENT '系统路由分组唯一标识',
    `is_demo`           tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为系统demo。0否，1是',
    `create_time`       int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time`  int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`             tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_system_key` (`system_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '后台路由分组表' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `__PREFIX__admin_route_request`;
CREATE TABLE `__PREFIX__admin_route_request`
(
    `id`                int(10) unsigned NOT NULL AUTO_INCREMENT,
    `route_id`          int(11) NOT NULL DEFAULT '0' COMMENT '路由id',
    `desc`              varchar(64)  NOT NULL DEFAULT '' COMMENT '描述',
    `full_desc`         varchar(255) NOT NULL DEFAULT '' COMMENT '完整描述，父级路由描述拼接',
    `method`            varchar(8)   NOT NULL DEFAULT '' COMMENT '请求method',
    `require_params`    varchar(200) NOT NULL DEFAULT '' COMMENT '必要参数',
    `enable_login_auth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '登录鉴权。0未配置，1启用，2关闭',
    `sort`              int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `system_key`        varchar(64)           DEFAULT NULL COMMENT '系统路由请求唯一标识',
    `is_demo`           tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为系统demo。0否，1是',
    `create_time`       int(11) unsigned NOT NULL COMMENT '创建时间',
    `last_update_time`  int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    `state`             tinyint(4) unsigned NOT NULL DEFAULT '11' COMMENT '11 正常 | 22 回收站 | 33 已删除',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `uk_system_key` (`system_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '后台路由请求项表' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `__PREFIX__setting`;
CREATE TABLE `__PREFIX__setting`
(
    `id`               int(11) unsigned NOT NULL AUTO_INCREMENT,
    `group`            varchar(100) NOT NULL DEFAULT '' COMMENT '配置分组',
    `key`              varchar(100) NOT NULL DEFAULT '' COMMENT '配置的键名',
    `value`            text COMMENT '配置的键值',
    `create_time`      int(11) unsigned NOT NULL,
    `last_update_time` int(11) unsigned NOT NULL COMMENT '最后一次更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `uk` (`group`,`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '配置表' ROW_FORMAT = Dynamic;
