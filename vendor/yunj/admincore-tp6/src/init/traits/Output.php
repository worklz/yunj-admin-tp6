<?php

namespace yunj\init\traits;

trait Output {

    // 处理输出
    protected function handleOutput(string &$output): void {
        $output = str_replace([root_path()], [''], $output);
    }

    // 问题
    public function confirm(string $question) {
        $this->handleOutput($question);
        // 输出一个提示
        $text = sprintf('%s [Y/N]: ', $question);
        fwrite(STDOUT, $text);
        $res = trim(fgets(STDIN));
        if (!$res) return false;
        $res = strtolower($res);
        // 接受输入
        return $res === 'y' || $res === 'yes';
    }

    // 询问
    public function ask(string $desc) {
        $this->handleOutput($desc);
        // 输出一个提示
        $text = sprintf('%s: ', $desc);
        fwrite(STDOUT, $text);
        // 接受输入
        return trim(fgets(STDIN));
    }

    // 常规消息
    public function info(string $message, bool $nextLine = true) {
        $this->handleOutput($message);
        $text = $message . ($nextLine ? "\r\n" : '');
        fwrite(STDOUT, $text);
    }

    // 警告消息
    public function warning(string $message, bool $nextLine = true) {
        $this->handleOutput($message);
        $text = sprintf("\033[%smWarning! %s\033[%sm", '33', $message, '39') . ($nextLine ? "\r\n" : '');
        fwrite(STDOUT, $text);
    }

    // 跳过消息
    public function skip(string $message, bool $nextLine = true) {
        $this->handleOutput($message);
        $text = sprintf("\033[%smSkip! %s\033[%sm", '34', $message, '39') . ($nextLine ? "\r\n" : '');
        fwrite(STDOUT, $text);
    }

    // 成功消息
    public function success(string $message, bool $nextLine = false, bool $prefix = true) {
        $this->handleOutput($message);
        if ($prefix) $message = "Success! " . $message;
        $text = sprintf("\033[%sm%s\033[%sm", '32', $message, '39') . ($nextLine ? "\r\n" : '');
        fwrite(STDOUT, $text);
    }

    // 错误消息
    public function error($err, bool $nextLine = false) {
        if ($err instanceof \Throwable) $err = exception_to_str($err);
        else $this->handleOutput($err);
        $text = sprintf("\033[%smError: %s\033[%sm", '31', $err, '39') . ($nextLine ? "\r\n" : '');
        fwrite(STDOUT, $text);
    }

}