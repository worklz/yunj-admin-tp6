<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 后台常规配置，默认文件内容
// +----------------------------------------------------------------------

return [
    "use_demo" => <<<INFO
    // 引入demo
    // 此处调整后执行权限、路由同步命令（或调用页面同步功能）
    'use_demo' => true,
INFO
    ,
    "entrance" => <<<INFO
    // 管理入口
    // 后台安全入口,设置后只能通过指定安全入口登录后台,如: admin。注意留空默认为：admin'
    'entrance' => 'admin',
INFO
    ,
    "dashboard_url" => <<<INFO
    // 桌面仪表盘地址
    // 作为后台桌面地址，该地址页面不可关闭
    'dashboard_url' => '/admin/yunj/demo/welcome',
INFO
    ,
    "login_controller" => <<<INFO
    // 登录控制器
    // 需实现接口：\yunj\app\admin\controller\member\MemberLoginControllerInterface
    // 配置完成后需进入项目根目录，执行命令：php think yunj:init-system-routes 重置系统路由（或进入路由管理页面执行列表右上角系统路由同步功能）
    'login_controller'=>'\yunj\app\admin\controller\member\MemberLoginController',
INFO
    ,
    "style_file_list" => <<<INFO
    // 页面加载的css文件路径/地址，如：'/test_1.css,/test_2.css'
    'style_file_list' => [],
INFO
    ,
    "script_file_list" => <<<INFO
    // 页面加载的js文件路径/地址，如：'/test_1.js,/test_2.js'
    'script_file_list' => [],
INFO
    ,
    "theme" => <<<INFO
    /**
     * 自定义主题配置
     * 示例：
     *  'theme'=>[
     *      // 主题唯一code
     *      'default'=>[
     *          // 主题标题
     *          'title'=>'默认主题',
     *          // 主题模板样式文件路径
     *          'tpl_style_file'=>'/static/yunj/css/theme/default/tpl.css',
     *          // 主题样式文件路径/static/
     *          'style_file'=>'/static/yunj/css/theme/default/default.css',
     *      ],
     *  ],
     */
    'theme' => [
        'example' => [
            'title' => '示例主题',
            'tpl_style_file' => '/static/yunj/demo/css/theme/example/tpl.css?v=1.0.2',
            'style_file' => '/static/yunj/demo/css/theme/example/example.css?v=1.0.2',
        ],
    ],
INFO
    ,
];