<?php

namespace yunj\app\demo\enum;

abstract class Enum extends \yunj\core\enum\Enum {

    public static function getTitleMap(): array {
        return static::getAllEnumAttrOptions('title');
    }

    public function getTitle(){
        return $this->getAttr('title');
    }

    public static function getValueByTitle(string $title){
        return array_search($title,static::getTitleMap(),true);
    }

}