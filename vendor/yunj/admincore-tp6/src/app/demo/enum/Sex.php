<?php

namespace yunj\app\demo\enum;

final class Sex extends Enum {

    const MAN = "man";

    const WOMAN = "woman";

    public static function allEnumAttrs(): array {
        return [
            self::MAN=>[
                'title'=>'男',
            ],
            self::WOMAN=>[
                'title'=>'女',
            ]
        ];
    }

}