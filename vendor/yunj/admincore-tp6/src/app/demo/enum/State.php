<?php

namespace yunj\app\demo\enum;

final class State extends Enum {

    const NORMAL = 11;

    const RECYLE_BIN = 22;

    const DELETED = 33;

    public static function allEnumAttrs(): array {
        return [
            self::NORMAL=>[
                'title'=>'正常',
            ],
            self::RECYLE_BIN=>[
                'title'=>'回收站',
            ],
            self::DELETED=>[
                'title'=>'已删除',
            ]
        ];
    }

    public static function getTableState() {
        $options = [];
        $allEnumAttrs = self::allEnumAttrs();
        foreach ($allEnumAttrs as $key => $val) {
            if ($key == self::DELETED) {
                continue;
            }
            $options[] = ['key' => $key, 'title' => $val['title'] ?? ''];
        }
        return $options;
    }

}