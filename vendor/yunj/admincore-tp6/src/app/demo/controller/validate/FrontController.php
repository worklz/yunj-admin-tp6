<?php

namespace yunj\app\demo\controller\validate;

use yunj\app\demo\controller\Controller;

class FrontController extends Controller {

    public function index() {
        return $this->view("validate/front/index");
    }

}