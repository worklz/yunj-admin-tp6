<?php

namespace yunj\app\demo\controller\form;

use yunj\app\demo\controller\Controller;

class SingleController extends Controller {

    public function index() {
        return $this->view("form/single/index");
    }

}