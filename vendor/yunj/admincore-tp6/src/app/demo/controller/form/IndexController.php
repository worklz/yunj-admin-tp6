<?php

namespace yunj\app\demo\controller\form;

use yunj\app\demo\controller\Controller;
use yunj\app\demo\service\form\Index as Service;
use yunj\core\builder\YunjForm;

class IndexController extends Controller {

    // 简单示例
    public function sample() {
        $builder = YF('demo')
            ->field(function (YunjForm $builder, $tab) {
                return [
                    "id" => [
                        "type" => "hidden"
                    ],
                    "markdown" => [
                        "title" => "Markdown",
                        "type" => "markdown",
                        "default" => "123123"
                    ],
                    "name" => [
                        "title" => "姓名",
                        "type" => "text",
                        "placeholder" => "请输入汉字姓名",
                        "verify" => 'require|chs',
                        "grid" => [12, 6, 6]
                    ],
                    "age" => [
                        "title" => "年龄",
                        "type" => "text",
                        "verify" => 'require|positiveInt',
                        "grid" => [12, 6, 6]
                    ],
                    "sex" => [
                        "title" => "性别",
                        "type" => "radio",
                        "options" => [
                            'male' => '男',
                            'female' => '女',
                        ],
                        "grid" => [12, 6, 6]
                    ],
                ];
            })
            ->button(['reset', 'submit'])
            ->load(function () {
                // ...业务代码
                return ['id' => 999, 'name' => '小王', 'age' => 18, 'sex' => 'male'];
            })
            ->submit(function (YunjForm $builder, $data) {
                // ...业务代码
                return success_json();
            });
        return view_form($builder);
    }

    public function chain() {
        $builder = YF('demo')
            ->tab(Service::getInstance()->tab())
            ->field(function (YunjForm $builder, $tab) {
                return Service::getInstance()->field($tab);
            })
            ->button(['reload', 'reset', 'submit'])
            ->submit(function (YunjForm $builder, $data) {
                // $data 表单数据

                // ...业务代码

                // 返回相应结果
                //return success_json();
                return success_json($data);
            });
        $builder->assign();
        return $this->view("form/index/chain");
    }

    public function arrayConfig() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function (YunjForm $builder, $tab) {
                return Service::getInstance()->field($tab);
            },
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function (YunjForm $builder, $data) {
                // $data 表单数据

                // ...业务代码

                // 返回相应结果
                //return success_json();
                return success_json($data);
            },
        ]);
        $builder->assign();
        return $this->view("form/index/array_config");
    }

    public function dropdownSearchOptions() {
        $items = Service::getInstance()->dropdownSearchOptions();
        return success_json($items);
    }

}