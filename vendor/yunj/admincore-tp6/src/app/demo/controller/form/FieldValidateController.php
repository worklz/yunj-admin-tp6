<?php

namespace yunj\app\demo\controller\form;

use yunj\app\demo\controller\Controller;
use yunj\app\demo\validate\form\FieldValidate as Validate;
use yunj\core\builder\YunjForm;

class FieldValidateController extends Controller {

    public function index() {
        $builder = YF('demo', [
            "tab" => ["base" => "基础"],
            "field" => function (YunjForm $builder,$tab) {
                $field = [
                    "name" => [
                        "title" => "姓名",
                        "type" => "text",
                        "verify" => "require",
                        "desc" => "必填",
                    ],
                    "desc" => [
                        "title" => "介绍",
                        "type" => "textarea",
                        "verify" => "max:200",
                        "desc" => "限定200字符",
                    ],
                    "username" => [
                        "title" => "账户",
                        "type" => "text",
                        "verify" => "require|alphaDash",
                        "desc" => "必填，限定字母/数字/下划线_及短横线-组合",
                    ],
                    "custom" => [
                        "title" => "自定义字段",
                        "type" => "text",
                        "verify" => "checkCustom",
                        "desc" => "自定义方法checkCustom，校验在输入内容时只能是：123456",
                    ]
                ];
                return $field;
            },
            "fieldValidate" => Validate::class,
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function (YunjForm $builder,$data) {
                return success_json();
            },
        ]);
        $builder->assign();
        return $this->view("form/field_validate/index");
    }

    public function codePreview(){
        return $this->view("form/field_validate/code_preview");
    }

}