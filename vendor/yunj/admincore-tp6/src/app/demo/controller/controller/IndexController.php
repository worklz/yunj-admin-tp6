<?php

namespace yunj\app\demo\controller\controller;

use yunj\app\demo\controller\Controller;

class IndexController extends Controller {

    public function instruction() {
        return $this->view("controller/index/instruction");
    }

}