<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 成员模块权限配置
// +----------------------------------------------------------------------

use yunj\app\admin\enum\State;
use yunj\app\admin\enum\IsDemo;
use yunj\core\constants\BuilderConst;
use yunj\app\admin\enum\auth\AuthType;
use yunj\app\admin\enum\auth\AuthPageOpen;
use yunj\app\admin\enum\route\RequestMethod;

/**
 * 权限项
 * 'key' => [
 *       'name' => '',                  // 描述
 *       'icon' => '',                  // 图标
 *       'parent' => '',                // 对应父级key
 *       'type' => AuthType,            // 类型
 *       'page_open' => AuthPageOpen,   // 页面打开方式
 *       'url' => ['', '', []],         // 地址对应 [需要判断权限时:基础地址不含域名和QUERY_STRING；仅为外部跳转链接时:全地址,请求方式method默认get,必须参数]
 *       'layout' => null,              // 重写权限html结构
 *   ],...
 */

// 补充type
function __supp_type(array &$auth, AuthType $authType) {
    if ($auth['isMenu'] ?? false) {
        $auth['type'] = $authType->getValue();
    }
}

// 补充url必须参数
function __supp_url_require_params(array &$auth, AuthType $authType) {
    if (!isset($auth['url'])) {
        return;
    }
    if (!is_array($auth['url'])) {
        $auth['url'] = [$auth['url']];
    }
    $auth['url'][1] = $auth['url'][1] ?? '';
    $auth['url'][2] = $auth['url'][2] ?? [];
    if (substr($auth['url'][0], 0, 4) != 'http') {
        $auth['url'][2]['originLocation'] = $authType->getValue();
    }
}

// 补充parent参数
function __supp_parent(array &$auth, AuthType $authType) {
    if (isset($auth['parent']) && $auth['parent']) {
        $auth['parent'] = $authType->getStrVal() . '_' . $auth['parent'];
    }
}

// 补充key
function __supp_key(string $key, AuthType $authType): string {
    return $authType->getStrVal() . '_' . $key;
}

// 处理构建器权限
function __handle_builder_auths(array $auths, AuthType $authType, ?callable $handleParent = null): array {
    $newAuths = [];
    foreach ($auths as $k => $auth) {
        // 处理parent
        if (is_callable($handleParent)) {
            $auth = call_user_func_array($handleParent, [$k, $auth]);
        }
        // 补充type
        __supp_type($auth, $authType);
        // 补充parent
        __supp_parent($auth, $authType);
        // 补充url必须参数
        __supp_url_require_params($auth, $authType);
        // 补充key
        $key = __supp_key($k, $authType);
        $newAuths[$key] = $auth;
    }
    return $newAuths;
}

// 构建器权限
$builderAuths = [
    // 表单构建器
    'yunj_demo_form_builder_manage' => [
        'name' => '表单构建器',
        'icon' => 'layui-icon-form',
        'isMenu' => true,
    ],
    'yunj_demo_form_builder_sample_page' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '简单示例',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoFormBuilderSample'), RequestMethod::GET]
    ],
    'yunj_demo_form_builder_sample_save' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '简单示例数据保存',
        'url' => [build_url('yunjDemoFormBuilderSample'), RequestMethod::POST]
    ],
    'yunj_demo_form_builder_chain_page' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '链式操作',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoFormBuilderChain'), RequestMethod::GET]
    ],
    'yunj_demo_form_builder_chain_save' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '链式操作数据保存',
        'url' => [build_url('yunjDemoFormBuilderChain'), RequestMethod::POST]
    ],
    'yunj_demo_form_builder_array_config_page' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '数组配置',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoFormBuilderArrayConfig'), RequestMethod::GET]
    ],
    'yunj_demo_form_builder_array_config_save' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '数组配置数据提交',
        'url' => [build_url('yunjDemoFormBuilderArrayConfig'), RequestMethod::POST]
    ],
    'yunj_demo_form_builder_array_config_popup_page' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '弹窗打开',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::POPUP,
        'isMenu' => true,
        'url' => [build_url('yunjDemoFormBuilderArrayConfig'), RequestMethod::GET, ['isPopup' => 'yes']]
    ],
    'yunj_demo_form_builder_array_config_popup_save' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '弹窗打开数据保存',
        'url' => [build_url('yunjDemoFormBuilderArrayConfig'), RequestMethod::POST, ['isPopup' => 'yes']]
    ],
    'yunj_demo_form_builder_field_validate_page' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '字段验证器使用',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoFormBuilderFieldValidate'), RequestMethod::GET]
    ],
    'yunj_demo_form_builder_field_validate_save' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '字段验证器使用数据提交',
        'url' => [build_url('yunjDemoFormBuilderFieldValidate'), RequestMethod::POST]
    ],
    'yunj_demo_form_builder_single' => [
        'parent' => 'yunj_demo_form_builder_manage',
        'name' => '单一字段调用',
        'icon' => 'layui-icon-form',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoFormBuilderSingle')
    ],
    // 表格构建器
    'yunj_demo_table_builder_manage' => [
        'name' => '表格构建器',
        'icon' => 'layui-icon-table',
        'isMenu' => true,
    ],
    'yunj_demo_table_builder_sample' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '简单示例',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderSample')
    ],
    'yunj_demo_table_builder_chain' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '链式操作',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderChain')
    ],
    'yunj_demo_table_builder_array_config' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '数组配置',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderArrayConfig')
    ],
    'yunj_demo_table_builder_true' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '树形表格',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderTree')
    ],
    'yunj_demo_table_builder_array_config_1' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '弹窗打开',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::POPUP,
        'isMenu' => true,
        'url' => [build_url('yunjDemoTableBuilderArrayConfig'), RequestMethod::GET, ['isPopup' => 'yes']]
    ],
    'yunj_demo_table_builder_drag_sort' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '拖拽排序',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderDragSort')
    ],
    'yunj_demo_table_builder_drag_sort_handle' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '拖拽排序事件处理',
        'url' => [
            build_url('yunjDemoTableBuilderDragSort'), RequestMethod::POST, [
                BuilderConst::ASYNC_TYPE_KEY => 'event',
                BuilderConst::ASYNC_EVENT_KEY => 'sort',
            ]
        ]
    ],
    'yunj_demo_table_builder_pk' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '自定义主键字段',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => build_url('yunjDemoTableBuilderPk')
    ],
    'yunj_demo_table_builder_pk_handle' => [
        'parent' => 'yunj_demo_table_builder_manage',
        'name' => '自定义主键字段后端获取数据主键',
        'url' => [
            build_url('yunjDemoTableBuilderPk'), RequestMethod::POST, [
                BuilderConst::ASYNC_TYPE_KEY => 'event',
                BuilderConst::ASYNC_EVENT_KEY => 'back_get_pk',
            ]
        ]
    ],
    // 导入构建器
    'yunj_demo_import_builder_manage' => [
        'name' => '导入构建器',
        'icon' => 'layui-icon-upload',
        'isMenu' => true,
    ],
    'yunj_demo_import_builder_sample_page' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '简单示例',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoImportBuilderSample'), RequestMethod::GET]
    ],
    'yunj_demo_import_builder_sample_handle' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '简单示例数据导入',
        'url' => [build_url('yunjDemoImportBuilderSample'), RequestMethod::POST]
    ],
    'yunj_demo_import_builder_chain_page' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '链式操作',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoImportBuilderChain'), RequestMethod::GET]
    ],
    'yunj_demo_import_builder_chain_handle' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '链式操作数据导入',
        'url' => [build_url('yunjDemoImportBuilderChain'), RequestMethod::POST]
    ],
    'yunj_demo_import_builder_array_config_page' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '数组配置',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::TAB,
        'isMenu' => true,
        'url' => [build_url('yunjDemoImportBuilderArrayConfig'), RequestMethod::GET]
    ],
    'yunj_demo_import_builder_array_config_handle' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '数组配置数据导入',
        'url' => [build_url('yunjDemoImportBuilderArrayConfig'), RequestMethod::POST]
    ],
    'yunj_demo_import_builder_array_config_popup_page' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '弹窗打开',
        'icon' => 'layui-icon-table',
        'page_open' => AuthPageOpen::POPUP,
        'isMenu' => true,
        'url' => [build_url('yunjDemoImportBuilderArrayConfig'), RequestMethod::GET, ['isPopup' => 'yes']]
    ],
    'yunj_demo_import_builder_array_config_popup_handle' => [
        'parent' => 'yunj_demo_import_builder_manage',
        'name' => '弹窗打开数据导入',
        'url' => [build_url('yunjDemoImportBuilderArrayConfig'), RequestMethod::POST, ['isPopup' => 'yes']]
    ],
];

// 顶部权限
/** @var AuthType $topAuthTypeObj */
$topAuthTypeObj = AuthType::TOP_MENU();

$topAuths = [
    'yunj_demo_add' => [
        'name' => '+ 用户新增',
        'type' => AuthType::TOP_MENU,
    ],
    'yunj_demo_add_user_popup_page' => [
        'name' => '新增用户页面查看-弹窗',
        'parent' => 'yunj_demo_add',
        'type' => AuthType::TOP_MENU,
        'page_open' => AuthPageOpen::POPUP,
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::GET, ['isPopup' => 'yes', 'originLocation' => $topAuthTypeObj->getValue()]]
    ],
    'yunj_demo_add_user_popup_save' => [
        'name' => '新增用户数据保存-弹窗 数据保存',
        'parent' => 'yunj_demo_add',
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::POST, ['isPopup' => 'yes', 'originLocation' => $topAuthTypeObj->getValue()]]
    ],
    'yunj_demo_add_user_tab_page' => [
        'name' => '新增用户页面查看-子页面',
        'parent' => 'yunj_demo_add',
        'type' => AuthType::TOP_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::GET, ['originLocation' => $topAuthTypeObj->getValue()]]
    ],
    'yunj_demo_add_user_tab_save' => [
        'name' => '新增用户页面查看-子页面 数据保存',
        'parent' => 'yunj_demo_add',
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::POST, ['originLocation' => $topAuthTypeObj->getValue()]]
    ],
    // 开发文档
    'yunj_demo_help' => [
        'name' => '开发文档',
        'icon' => 'layui-icon-read',
        'type' => AuthType::TOP_MENU,
        'page_open' => AuthPageOpen::NEW,
        'url' => 'http://tp6admin.doc.iyunj.cn'
    ],
    // 源码地址
    'yunj_demo_code' => [
        'name' => '源码地址',
        'icon' => 'layui-icon-fonts-code',
        'type' => AuthType::TOP_MENU,
        'page_open' => AuthPageOpen::NEW,
        'url' => 'https://gitee.com/worklz/yunj-admin-tp6'
    ],
    // 问题反馈
    'yunj_demo_issue' => [
        'name' => '问题反馈',
        'icon' => 'layui-icon-survey',
        'type' => AuthType::TOP_MENU,
        'page_open' => AuthPageOpen::NEW,
        'url' => 'https://gitee.com/worklz/yunj-admin-tp6/issues'
    ],
];
$topBuilderAuths = __handle_builder_auths($builderAuths, $topAuthTypeObj);
array_insert($topAuths, $topBuilderAuths, 'yunj_demo_help');

// 侧边栏用户demo菜单权限
$sidebarDemoUserMenuAuths = [
    // 用户管理demo
    'yunj_demo_user' => [
        'parent' => '',
        'name' => 'DEMO用户管理',
        'icon' => 'layui-icon-user',
        'type' => AuthType::SIDEBAR_MENU
    ],
    // 用户列表
    'yunj_demo_user_list' => [
        'parent' => 'yunj_demo_user',
        'name' => '用户列表',
        'icon' => 'layui-icon-list',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjDemoUserList'), RequestMethod::GET]
    ],
    // 用户列表查看
    'yunj_demo_user_list_view' => [
        'name' => '查看',
        'parent' => 'yunj_demo_user_list',
        'url' => [build_url('yunjDemoUserList'), RequestMethod::POST],
    ],
    'yunj_demo_user_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_album' => [
        'name' => '相册',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_hobby' => [
        'name' => '爱好',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_interest_cate_ids' => [
        'name' => '兴趣分类',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_area' => [
        'name' => '地址',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_document' => [
        'name' => '个人文档',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_other_document' => [
        'name' => '其他文档',
        'parent' => 'yunj_demo_user_list_view',
    ],
    'yunj_demo_user_list_view_test' => [
        'name' => '自定义表头',
        'parent' => 'yunj_demo_user_list_view',
    ],
    // 用户列表操作
    'yunj_demo_user_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_demo_user_list',
    ],
    // 用户新增
    'yunj_demo_user_add_page' => [
        'name' => '新增页面查看',
        'parent' => 'yunj_demo_user_list_action',
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::GET]
    ],
    'yunj_demo_user_add_save' => [
        'name' => '新增数据保存',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserAdd'), RequestMethod::POST]
    ],
    // 用户编辑
    'yunj_demo_user_edit' => [
        'name' => '编辑',
        'parent' => 'yunj_demo_user_list_action',
    ],
    // 用户编辑详情
    'yunj_demo_user_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_demo_user_edit',
        'url' => [build_url('yunjDemoUserEdit'), RequestMethod::GET]
    ],
    'yunj_demo_user_edit_detail_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_hobby' => [
        'name' => '爱好',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_area' => [
        'name' => '地址',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_test' => [
        'name' => '测试',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_interest_cate_ids' => [
        'name' => '感兴趣的分类',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_friend_ids' => [
        'name' => '朋友',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_color' => [
        'name' => '喜欢的颜色',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_album' => [
        'name' => '相册',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_document' => [
        'name' => '个人文档',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    'yunj_demo_user_edit_detail_other_document' => [
        'name' => '其他文档',
        'parent' => 'yunj_demo_user_edit_detail',
    ],
    // 用户编辑提交
    'yunj_demo_user_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_demo_user_edit',
        'url' => [build_url('yunjDemoUserEdit'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'submit']]
    ],
    // 移入回收站
    'yunj_demo_user_recyle_bin' => [
        'name' => '移入回收站',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserList'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => State::RECYLE_BIN]]
    ],
    'yunj_demo_user_normal' => [
        'name' => '恢复正常',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserList'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => State::NORMAL]]
    ],
    'yunj_demo_user_deleted' => [
        'name' => '永久删除',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserList'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => State::DELETED]]
    ],
    // 用户导出
    'yunj_demo_user_export' => [
        'name' => '导出',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserList'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
    // 用户导入
    'yunj_demo_user_import' => [
        'name' => '导入',
        'parent' => 'yunj_demo_user_list_action',
        'url' => [build_url('yunjDemoUserImport'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'import']]
    ],
    // 所有用户
    'yunj_demo_all_user_list' => [
        'parent' => 'yunj_demo_user',
        'name' => '所有用户',
        'icon' => 'layui-icon-list',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjDemoAllUserList'), RequestMethod::GET]
    ],
    // 所有用户查看
    'yunj_demo_all_user_list_view' => [
        'name' => '列表查看',
        'parent' => 'yunj_demo_all_user_list',
        'url' => [build_url('yunjDemoAllUserList'), RequestMethod::POST],
    ],
    'yunj_demo_all_user_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_album' => [
        'name' => '相册',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_hobby' => [
        'name' => '爱好',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_interest_cate_ids' => [
        'name' => '兴趣分类',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_area' => [
        'name' => '地址',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_document' => [
        'name' => '个人文档',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    'yunj_demo_all_user_list_view_other_document' => [
        'name' => '其他文档',
        'parent' => 'yunj_demo_all_user_list_view',
    ],
    // 所有用户操作
    'yunj_demo_all_user_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_demo_all_user_list',
    ],
    // 用户编辑
    'yunj_demo_all_user_edit' => [
        'name' => '编辑',
        'parent' => 'yunj_demo_all_user_list_action',
    ],
    'yunj_demo_all_user_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_demo_all_user_edit',
        'url' => [build_url('yunjDemoAllUserEdit'), RequestMethod::GET]
    ],
    'yunj_demo_all_user_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_demo_all_user_edit',
        'url' => [build_url('yunjDemoAllUserEdit'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'submit']]
    ],
    // 用户导出
    'yunj_demo_all_user_export' => [
        'name' => '导出',
        'parent' => 'yunj_demo_all_user_list_action',
        'url' => [build_url('yunjDemoAllUserList'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
    // 用户导入
    'yunj_demo_all_user_import' => [
        'name' => '导入',
        'parent' => 'yunj_demo_all_user_list_action',
        'url' => [build_url('yunjDemoAllUserImport'), RequestMethod::POST, [BuilderConst::ASYNC_TYPE_KEY => 'import']]
    ],
];

// 侧边栏权限
$sidebarBuilderAuths = __handle_builder_auths($builderAuths, AuthType::SIDEBAR_MENU(), function (string $key, array $auth) {
//    if ($key != 'yunj_demo' && !($parent = $auth['parent'] ?? '')) {
//        $auth['parent'] = 'yunj_demo';
//    }
    return $auth;
});
$sidebarAuths = [
        // 控制器
        'yunj_demo_controller_manage' => [
            'parent' => '',
            'name' => '控制器',
            'icon' => 'layui-icon-find-fill',
            'type' => AuthType::SIDEBAR_MENU
        ],
        'yunj_demo_controller_index' => [
            'parent' => 'yunj_demo_controller_manage',
            'name' => '使用说明',
            'icon' => 'layui-icon-form',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoControllerInstruction')
        ],
        // 视图模板
        'yunj_demo_view_manage' => [
            'parent' => '',
            'name' => '视图模板',
            'icon' => 'layui-icon-template-1',
            'type' => AuthType::SIDEBAR_MENU
        ],
        'yunj_demo_view_index' => [
            'parent' => 'yunj_demo_view_manage',
            'name' => '使用说明',
            'icon' => 'layui-icon-form',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewInstruction')
        ],
        'yunj_demo_view_block' => [
            'parent' => 'yunj_demo_view_manage',
            'name' => '区块重写',
            'icon' => 'layui-icon-layouts',
            'type' => AuthType::SIDEBAR_MENU,
        ],
        'yunj_demo_view_block_seo' => [
            'parent' => 'yunj_demo_view_block',
            'name' => 'seo',
            'icon' => 'layui-icon-app',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewBlockSeo')
        ],
        'yunj_demo_view_block_head_style' => [
            'parent' => 'yunj_demo_view_block',
            'name' => 'headStyle',
            'icon' => 'layui-icon-app',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewBlockHeadStyle')
        ],
        'yunj_demo_view_block_head_script' => [
            'parent' => 'yunj_demo_view_block',
            'name' => 'headScript',
            'icon' => 'layui-icon-app',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewBlockHeadScript')
        ],
        'yunj_demo_view_block_content' => [
            'parent' => 'yunj_demo_view_block',
            'name' => 'content',
            'icon' => 'layui-icon-app',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewBlockContent')
        ],
        'yunj_demo_view_block_script' => [
            'parent' => 'yunj_demo_view_block',
            'name' => 'script',
            'icon' => 'layui-icon-app',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewBlockScript')
        ],
        'yunj_demo_view_icon' => [
            'parent' => 'yunj_demo_view_manage',
            'name' => '图标',
            'icon' => 'layui-icon-flag',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoViewIcon')
        ],
        // 验证器
        'yunj_demo_validate_manage' => [
            'parent' => '',
            'name' => '验证器',
            'icon' => 'layui-icon-survey',
            'type' => AuthType::SIDEBAR_MENU
        ],
        'yunj_demo_validate_tp_page' => [
            'parent' => 'yunj_demo_validate_manage',
            'name' => 'TP验证器页面',
            'icon' => 'layui-icon-form',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => [build_url('yunjDemoValidateTp'), RequestMethod::GET]
        ],
        'yunj_demo_validate_tp_handle' => [
            'parent' => 'yunj_demo_validate_manage',
            'name' => 'TP验证器数据处理',
            'url' => [build_url('yunjDemoValidateTp'), RequestMethod::POST]
        ],
        'yunj_demo_validate_front' => [
            'parent' => 'yunj_demo_validate_manage',
            'name' => '前端验证器',
            'icon' => 'layui-icon-form',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::TAB,
            'url' => build_url('yunjDemoValidateFront')
        ],
        'yunj_demo_validate_rule' => [
            'parent' => 'yunj_demo_validate_manage',
            'name' => '通用验证规则',
            'icon' => 'layui-icon-form',
            'type' => AuthType::SIDEBAR_MENU,
            'page_open' => AuthPageOpen::NEW,
            'url' => '//tp6admin.doc.iyunj.cn/validate-rule.html'
        ],
    ] + $sidebarBuilderAuths + $sidebarDemoUserMenuAuths;

$auths = $topAuths + $sidebarAuths;
foreach ($auths as $k=>$auth) { // 此处不能使用&引用的方式，会导致流转到后面的文件调用，引发异常
    $auths[$k]['is_demo'] = IsDemo::YES;
}
return $auths;