<?php

namespace yunj\app\demo\validate\form;

use yunj\app\demo\validate\Validate;

class FieldValidate extends Validate {

    protected function checkCustom($value, string $rule, array $data, string $field, string $title) {
        return $value === "123456" ? true : "自定义字段只能为123456";
    }

}