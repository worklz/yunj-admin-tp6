<?php

namespace yunj\app\demo\service\validate;

use yunj\app\demo\validate\validate\Thinkphp as Validate;

final class Thinkphp {

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 处理数据
     * Author: Uncle-L
     * Date: 2021/11/13
     * Time: 13:44
     * @param array $param
     * @return bool|string
     */
    public function handleData(array $param = []) {
        $param = $param ?: input("post.");
        $data = ["name" => $param["name"], "age" => $param["age"]];
        $rules = ["name" => $param["nameRule"], "age" => $param["ageRule"]];
        $validate = new Validate();
        $validate->rule($rules,["name" => "姓名", "age" => "年龄"]);
        $res = $validate->check($data);
        return $res ? true : $validate->getError();
    }

}