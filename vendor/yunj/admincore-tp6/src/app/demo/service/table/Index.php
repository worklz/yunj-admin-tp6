<?php

namespace yunj\app\demo\service\table;

use yunj\app\demo\enum\State;

final class Index {

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 状态栏
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:33
     * @return array
     */
    public function state(): array {
        return [
            ['key' => 1, 'title' => '状态一'],
            ['key' => 2, 'title' => '状态二'],
        ];
    }

    /**
     * Notes: 表头
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:42
     * @param $state [状态]
     * @param bool $isDragSort [是否拖拽排序]
     * @return array
     */
    public function cols($state, $isDragSort = false): array {
        $cols = [
            'id' => ['title' => '文本1'],
            'text1' => ['title' => '文本1'],
            'text2' => ['title' => '文本2'],
            'enum' => [
                'title' => '枚举',
                "templet" => "enum",
                "options" => [
                    1 => "一年级",
                    2 => "二年级",
                    3 => "三年级",
                    4 => "四年级",
                ]
            ],
            'datetime' => ['title' => '时间日期', "templet" => "datetime"],
            'img' => ['title' => '单图', "templet" => "image"],
            'imgs' => ['title' => '多图', "templet" => "image"],
            'file' => ['title' => '单文件', "templet" => "file"],
            'files' => ['title' => '多文件', "templet" => "file", 'hide' => 'Y'],
            'color' => ['title' => '颜色', "templet" => "color"],
            'action' => [
                'title' => '操作',
                'templet' => 'action',
                'options' => [
                    'edit' => ['type' => 'openPopup', 'title' => '详情', 'class' => 'layui-icon-survey', 'url' => '....'],
                    State::NORMAL => ['type' => 'asyncEnent', 'title' => '还原', 'dropdown' => true],
                    State::RECYLE_BIN => ['type' => 'asyncEnent', 'title' => '移入回收站', 'dropdown' => true],
                    State::DELETED => ['type' => 'asyncEnent', 'title' => '永久删除', 'dropdown' => true],
                ]
            ]
        ];
        if ($isDragSort) {
            $cols["text1"]["templet"] = "dragSort";
            array_insert($cols, [
                "text2" => [
                    'title' => '文本2',
                    'templet' => 'dragSort'
                ],
                // 当 dragSortAction 没有值时，只展示排序图标
                "dragSortAction" => [
                    'title' => '排序',
                    'align' => 'center',
                    'templet' => 'dragSort'
                ],
            ], "enum");
        }
        return $cols;
    }

    /**
     * Notes: 数量
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:50
     * @param $filter [筛选条件]
     * @return int
     */
    public function count($filter): int {
        // 固定参数
        $state = $filter['state'];
        $ids = $filter['ids'];

        // 业务处理：根据当前筛选条件获取数据量

        return 5;
    }

    /**
     * Notes: 数量
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:50
     * @param $filter [筛选条件]
     * @return array
     */
    public function items($page, $pageSize, $filter, $sort): array {
        // 固定参数
        $state = $filter['state'];
        $ids = $filter['ids'];

        // 业务处理：根据当前筛选条件获取数据量
        $items = [];
        for ($i = 1; $i <= 10; $i++) {
            $items[] = [
                "id" => $state . '_' . $i,
                "test_id" => $i + 1,
                "text1" => "测试内容{$i}-1",
                "text2" => "测试内容{$i}-2",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => "/static/yunj/img/default.png",
                "files" => ["/static/yunj/img/default.png", "/static/yunj/img/bg.png"],
                "color" => "#fff000",
            ];
        }

        return $items;
    }


}