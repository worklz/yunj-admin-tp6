<?php

namespace yunj\app\demo\service\import;

final class Index {

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 工作表
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:33
     * @return array
     */
    public function sheet(): array {
        return ["Sheet1","Sheet2"];
    }

    /**
     * Notes: 表头
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:42
     * @param $sheet [工作表]
     * @return array
     */
    public function cols($sheet): array {
        $cols = [
            'name'=>[
                "title"=>'姓名',
                "default"=>"小王",
                "verify"=>"require",
                "desc"=>"必填",
            ],
            'age'=>[
                "title"=>'年龄',
                "default"=>18,
                "verify"=>"require|positiveInteger",
                "desc"=>"必填，必须为正整数",
            ],
            'sex'=>[
                "title"=>'性别',
                "default"=>"男",
                "verify"=>"require|in:男,女",
                "desc"=>"必填，可选值：男/女",
            ]
        ];
        return $cols;
    }

}