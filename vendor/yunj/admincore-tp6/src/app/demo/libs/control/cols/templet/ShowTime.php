<?php

namespace yunj\app\demo\libs\control\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class ShowTime extends YunjCol {

    // 定义额外配置项（无额外配置项可不写）
    protected static function defineExtraArgs(): array {
        return [
            'format' => 'Y-m-d H:i:s',  // 时间格式
        ];
    }

    // 处理配置项（不需要处理可不写）
    protected static function handleArgs(array $args): array {
        return $args;
    }

}