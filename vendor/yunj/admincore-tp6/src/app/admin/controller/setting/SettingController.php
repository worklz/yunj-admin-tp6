<?php

namespace yunj\app\admin\controller\setting;

use yunj\app\admin\controller\Controller;
use yunj\app\admin\service\setting\SettingService;

class SettingController extends Controller {

    /**
     * @return SettingService
     */
    private function getService(): SettingService {
        /** @var SettingService $service */
        $service = app(SettingService::class);
        return $service->setGroup();
    }

    public function index() {
        $builder = $this->getService()->getFormBuilder();
        $builder->assign();
        return view_form($builder);
    }

}