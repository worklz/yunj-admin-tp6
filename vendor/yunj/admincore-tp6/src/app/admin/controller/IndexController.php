<?php

namespace yunj\app\admin\controller;

use think\facade\Route;
use yunj\app\admin\service\auth\AuthService;
use yunj\app\admin\service\route\RouteRequestService;
use yunj\app\admin\service\route\RouteSyncSystemDataService;

class IndexController extends Controller {

    public function index() {
        //dd(AuthService::syncSystemDatas());die;
        //dd(admin_sidebar_menu_html_layout());
        //dd(request()->adminRouteRequestData);
        //log_info('123','',['qwe'=>123]);
        $member = $this->getMember();
        $vars = [
            'globalSearchItems' => [
                
            ],
            'memberId' => $member->id,
            'memberDesc' => $member->name ?: $member->username
        ];
        return $this->view('index', $vars);
    }

}