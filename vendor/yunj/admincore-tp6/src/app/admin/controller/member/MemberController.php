<?php

namespace yunj\app\admin\controller\member;

use yunj\app\admin\controller\Controller;
use yunj\app\admin\service\member\MemberService;

class MemberController extends Controller {

    /**
     * @return MemberService
     */
    private function getService(): MemberService {
        return app(MemberService::class);
    }

    public function lists() {
        $builder = $this->getService()->getListBuilder();
        $builder->assign();
        return view_table($builder);
    }

    public function add() {
        $builder = $this->getService()->getFormBuilder();
        $builder->assign();
        return view_form($builder);
    }

    public function edit() {
        $builder = $this->getService()->getFormBuilder(true);
        $builder->assign();
        return view_form($builder);
    }

    // 个人简介
    public function profile() {
        $builder = $this->getService()->getProfileBuilder();
        $builder->assign();
        return view_form($builder);
    }

    // 下拉搜索
    public function dropdownsearch() {
        $data=input('get.');
        // 输入关键字
        $keywords=$data['keywords'];
        // 使用 , 分割的初始值
        $ids=$data['ids'];
        // 进行数据查询
        $where=[];
        if($keywords) $where[]=['username|name','like','%'.$keywords.'%'];
        if($ids) $where[]=['id','in',$ids];
        $field=['id',"concat(name,'【',username,'】') as name"];
        $items= self::getAdminMemberModel()->getOwnRowsToArray($where,$field,[],1,20);
        // 返回数据
        return success_json($items);
    }

}