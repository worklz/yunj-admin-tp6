<?php

namespace yunj\app\admin\job;

use think\queue\Job as ThinkQueueJob;
use yunj\app\admin\service\member\log\LogSaveService;

class AdminMemberLogRecordJob extends Job {

    public function fire(ThinkQueueJob $job, array $log) {
        // 执行异步任务逻辑
        LogSaveService::handle($log);
        // 任务执行完毕后，删除任务
        $job->delete();
    }

}