<?php

namespace yunj\app\admin\middleware;

use yunj\app\admin\traits\ModelGet;

abstract class Middleware {

    use ModelGet;

}