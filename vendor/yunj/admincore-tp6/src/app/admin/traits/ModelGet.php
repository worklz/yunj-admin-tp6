<?php

namespace yunj\app\admin\traits;

use yunj\app\admin\model\AdminAuth;
use yunj\app\admin\model\AdminMemberLog;
use yunj\app\admin\model\AdminMemberLogAttr;
use yunj\app\admin\model\AdminMember;
use yunj\app\admin\model\AdminMemberRole;
use yunj\app\admin\model\AdminMemberRoleAttr;
use yunj\app\admin\model\AdminMemberRoleRelation;
use yunj\app\admin\model\AdminRoute;
use yunj\app\admin\model\AdminRouteGroup;
use yunj\app\admin\model\Setting;
use yunj\app\admin\model\AdminRouteRequest;

trait ModelGet {

    /**
     * @return AdminMember
     */
    public static function getAdminMemberModel(): AdminMember {
        return app(AdminMember::class);
    }

    /**
     * @return AdminMemberRole
     */
    public static function getAdminMemberRoleModel(): AdminMemberRole {
        return app(AdminMemberRole::class);
    }

    /**
     * @return AdminMemberRoleRelation
     */
    public static function getAdminMemberRoleRelationModel(): AdminMemberRoleRelation {
        return app(AdminMemberRoleRelation::class);
    }

    /**
     * @return Setting
     */
    public static function getSettingModel(): Setting {
        return app(Setting::class);
    }

    /**
     * @return AdminMemberLog
     */
    public static function getAdminMemberLogModel(): AdminMemberLog {
        return app(AdminMemberLog::class);
    }

    /**
     * @return AdminMemberLogAttr
     */
    public static function getAdminMemberLogAttrModel(): AdminMemberLogAttr {
        return app(AdminMemberLogAttr::class);
    }

    /**
     * @return AdminRoute
     */
    public static function getAdminRouteModel(): AdminRoute {
        return app(AdminRoute::class);
    }

    /**
     * @return AdminRouteGroup
     */
    public static function getAdminRouteGroupModel(): AdminRouteGroup {
        return app(AdminRouteGroup::class);
    }

    /**
     * @return AdminRouteRequest
     */
    public static function getAdminRouteRequestModel(): AdminRouteRequest {
        return app(AdminRouteRequest::class);
    }

    /**
     * @return AdminAuth
     */
    public static function getAdminAuthModel(): AdminAuth {
        return app(AdminAuth::class);
    }

}