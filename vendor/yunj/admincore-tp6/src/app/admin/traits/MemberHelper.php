<?php

namespace yunj\app\admin\traits;

use yunj\app\admin\model\AdminMember;
use yunj\app\admin\model\AdminMemberRole;

trait MemberHelper {

    /**
     * 当前管理员id
     * @var int
     */
    protected $memberId;

    /**
     * 当前管理员数据
     * @var AdminMember
     */
    protected $member;

    /**
     * @return int
     */
    public function getMemberId(): int {
        if (is_null($this->memberId)) {
            $this->setMemberId();
        }
        return $this->memberId;
    }

    /**
     * 设置当前管理员id
     * @return static
     */
    public function setMemberId() {
        $member = $this->getMember();
        $this->memberId = $member->id ?? 0;
        return $this;
    }

    /**
     * 获取当前管理员数据
     * @return AdminMember
     */
    public function getMember() {
        if (is_null($this->member)) {
            $this->setMember();
        }
        return $this->member;
    }

    /**
     * 设置当前管理员数据
     * @param AdminMember|null $member
     * @return static
     */
    public function setMember(?AdminMember $member = null) {
        $this->member = $member ?: (request()->member ?: false);
        $this->setMemberId();
        return $this;
    }

}