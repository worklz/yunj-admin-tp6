<?php

namespace yunj\app\admin\enum;

final class IsDemo extends Enum {

    const YES = 1;

    const NO = 0;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::YES => [
                'desc' => '是'
            ],
            self::NO => [
                'desc' => '否'
            ],
        ];
    }

}