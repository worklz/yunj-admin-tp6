<?php

namespace yunj\app\admin\enum\auth;

use yunj\app\admin\enum\Enum;

/**
 * 权限请求类型
 * Class AuthRequestType
 * @package yunj\app\admin\enum\auth
 */
final class AuthRequestType extends Enum {

    const REQUEST_ID = 11;

    const REQUEST_URL = 22;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::REQUEST_ID => [
                'title' => '路由请求项',
            ],
            self::REQUEST_URL => [
                'title' => '自定义请求地址',
            ],
        ];
    }

}