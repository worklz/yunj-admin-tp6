<?php

namespace yunj\app\admin\enum\auth;

use yunj\app\admin\enum\Enum;

/**
 * 权限类型
 * Class AuthType
 * @package yunj\app\admin\enum\auth
 */
final class AuthType extends Enum {

    const NORMAL = 0;

    const SIDEBAR_MENU = 11;

    const TOP_MENU = 22;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::NORMAL => [
                'title' => '常规',
                'strVal' => '',
                'color' => '#16b777',
            ],
            self::SIDEBAR_MENU => [
                'title' => '侧边栏菜单',
                'strVal' => 'sidebar',
                'color' => '#16baaa',
            ],
            self::TOP_MENU => [
                'title' => '顶部菜单',
                'strVal' => 'top',
                'color' => '#1e9fff',
            ],
        ];
    }

    // 获取字符串值
    public function getStrVal() {
        return $this->getAttr('strVal', '');
    }

    // 是否为菜单类型
    public function isMenu(): bool {
        return in_array($this->getValue(), [self::SIDEBAR_MENU, self::TOP_MENU]);
    }

}