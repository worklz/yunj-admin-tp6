<?php

namespace yunj\app\admin\enum\auth;

use yunj\app\admin\enum\Enum;

/**
 * 权限页面打开方式
 * Class AuthPageOpen
 * @package yunj\app\admin\enum\auth
 */
final class AuthPageOpen extends Enum {

    const NIL = 0;

    const TAB = 11;

    const POPUP = 22;

    const NEW = 33;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::NIL => [
                'title' => '无',
                'strVal' => '',
                'color' => '#cccccc',
            ],
            self::TAB => [
                'title' => '子页面',
                'strVal' => 'tab',
                'color' => '#16b777',
            ],
            self::POPUP => [
                'title' => '弹出层页面',
                'strVal' => 'popup',
                'color' => '#16baaa',
            ],
            self::NEW => [
                'title' => '浏览器新标签页',
                'strVal' => 'new',
                'color' => '#1e9fff',
            ],
        ];
    }

    // 获取字符串值
    public function getStrVal() {
        return $this->getAttr('strVal', '');
    }

    // 是否为内部页面
    public function isLoaclPage(): bool {
        return in_array($this->getValue(), [self::TAB, self::POPUP]);
    }

    // 是否为页面
    public function isPage(): bool {
        return in_array($this->getValue(), [self::TAB, self::POPUP, self::NEW]);
    }

}