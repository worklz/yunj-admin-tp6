<?php

namespace yunj\app\admin\enum;

final class IsSystem extends Enum {

    const YES = 1;

    const NO = 0;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::YES => [
                'desc' => '是',
                'color' => '#2f363c', // 用于枚举背景颜色等
            ],
            self::NO => [
                'desc' => '否',
                'color' => '#c2c2c2'
            ],
        ];
    }

}