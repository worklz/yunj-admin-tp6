<?php

namespace yunj\app\admin\enum\route;

use yunj\app\admin\enum\Enum;

/**
 * 路由请求项页面表单类型
 * Class RoutePageFormType
 * @package yunj\app\admin\enum
 */
final class RoutePageFormType extends Enum {

    const ROUTE = 'route';

    const ITEM = 'item';

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::ROUTE => [
                'desc' => '路由'
            ],
            self::ITEM => [
                'desc' => '功能项'
            ],
        ];
    }

}