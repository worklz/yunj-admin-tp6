<?php

namespace yunj\app\admin\enum\route;

use yunj\app\admin\enum\Enum;

final class RequestMethod extends Enum {

    const GET = 'GET';

    const POST = 'POST';

    const DELETE = 'DELETE';

    const PUT = 'PUT';

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::GET => [
                'desc' => 'GET'
            ],
            self::POST => [
                'desc' => 'POST'
            ],
            self::DELETE => [
                'desc' => 'DELETE'
            ],
            self::PUT => [
                'desc' => 'PUT'
            ],
        ];
    }

}