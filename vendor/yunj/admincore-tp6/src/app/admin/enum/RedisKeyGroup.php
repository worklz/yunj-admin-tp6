<?php

namespace yunj\app\admin\enum;

use yunj\core\enum\RedisKeyGroup as YunjRedisKeyGroup;
use yunj\core\traits\RedisKeyGroup as RedisKeyGroupTrait;

class RedisKeyGroup extends YunjRedisKeyGroup {

    // 后台路由缓存分组
    const ADMIN_ROUTE = 'admin_route';

    // 后台权限缓存分组
    const ADMIN_AUTH = 'admin_auth';

    protected function redisKeyEnum() {
        return RedisKey::class;
    }

    public static function allEnumAttrs(): array {
        return [
            self::ADMIN_ROUTE => [
                'desc' => '后台路由缓存分组',
                'parent' => '',
                // 所包含的key
                'keys' => [
                    RedisKey::ADMIN_ROUTES,
                    RedisKey::ADMIN_VALID_ROUTE_REQUEST_DATAS_BY_BASE_URL,
                    RedisKey::ADMIN_PAGE_BASE_URL_BY_CONTROLLER_ACTION,
                    RedisKey::ADMIN_ROUTE_EQUEST_DATA_BY_ID,
                    RedisKey::ADMIN_LOGIN_HANDLE_ROUTE_REQUEST_DATA,
                    RedisKey::ADMIN_LOGOUT_ROUTE_REQUEST_DATA,
                ],
            ],
            self::ADMIN_AUTH => [
                'desc' => '后台权限缓存分组',
                'parent' => '',
                // 所包含的key
                'keys' => [
                    RedisKey::ADMIN_AUTH_DATA_BY_REQUEST_ID,
                    RedisKey::ADMIN_ALL_AUTH_DATA,
                    RedisKey::ADMIN_ALL_AUTH_DATA_BY_EXCLUDE_DEMO,
                ],
            ]
        ];
    }

}
