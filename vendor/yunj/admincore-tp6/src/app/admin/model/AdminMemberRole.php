<?php

namespace yunj\app\admin\model;

class AdminMemberRole extends Model {

    // 定义与用户表的多对多关联
    public function roles() {
        // 中间表使用表名，使用模型名需继承 think\model\Pivot 导致中间表模型不能实例化
        return $this->belongsToMany(AdminMember::class, 'admin_member_role_relation', 'member_id', 'role_id');
    }

    /**
     * 获取权限key数组
     * @return array
     */
    public function getAuthsToArray(): array {
        return $this->auths ? json_decode($this->auths, true) : [];
    }

}