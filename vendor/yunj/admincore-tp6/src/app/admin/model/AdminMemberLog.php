<?php

namespace yunj\app\admin\model;

use yunj\core\traits\ModelSplit;

/**
 * 后台成员日志表，按月份分表
 */
class AdminMemberLog extends Model {

    use ModelSplit;

    protected $dateFormat = false;
}