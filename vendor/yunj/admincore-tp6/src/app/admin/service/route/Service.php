<?php

namespace yunj\app\admin\service\route;

use yunj\app\admin\enum\EnableLoginAuth;
use yunj\app\admin\service\Service as BaseService;

abstract class Service extends BaseService {

    const DEF_REQUEST_ITEN_ARGS = [
        'desc' => '',                       // 描述
        'method' => '',                     // 请求method
        'require_params' => [],             // 必要参数
        'enable_login_auth' => EnableLoginAuth::ON,         // 是否需要登录
    ];

}
