<?php

namespace yunj\app\admin\service\route\page;

use yunj\app\admin\enum\EnableLoginAuth;
use yunj\app\admin\enum\route\RequestMethod;
use yunj\app\admin\service\route\Service;

abstract class RouteFormService extends Service {

    // 获取分组的公共字段
    public function getGroupCommonField(): array {
        return [
            'name' => ['title' => '名称', 'verify' => 'require|max:60', 'desc' => '最多支持60位字符，作为路由前缀，如：admin'],
            'desc' => ['title' => '描述', 'verify' => 'require|max:60', 'desc' => '最多支持60位字符'],
            'namespace' => ['title' => '命名空间', 'verify' => 'max:60', 'desc' => '最多支持60位字符'],
            'middleware' => [
                'title' => '中间件',
                'type' => 'table',
                'cols' => [
                    ['title' => '中间件（类全限定名称）', 'field' => 'class'],
                    ['title' => "参数（多参数','分割）", 'field' => 'params'],
                ],
                'verify' => 'isMiddleware',
                'desc' => "中间件（类全限定名称）示例：\\app\\middleware\\Test1；参数（多参数','分割）示例：param1,param2"
            ],
        ];
    }

    // 获取路由的公共字段
    public function getRouteCommonField(): array {
        return [
            'rule' => ['title' => '路由规则', 'verify' => 'require|max:110', 'desc' => '最多支持110位字符，如：user/list'],
            'route' => ['title' => '路由地址', 'verify' => 'require|max:110', 'desc' => '最多支持110位字符；格式：控制器@方法，如：\\app\\controller\\User@lists'],
            'name' => ['title' => '别名', 'verify' => 'max:60', 'desc' => '最多支持60位字符，用于TP生成地址'],
            'desc' => ['title' => '描述', 'verify' => 'max:60', 'desc' => '最多支持60位字符'],
            'middleware' => [
                'title' => '中间件',
                'type' => 'table',
                'cols' => [
                    ['title' => '中间件（类全限定名称）', 'field' => 'class'],
                    ['title' => "参数（多参数','分割）", 'field' => 'params'],
                ],
                'verify' => 'isMiddleware',
                'desc' => "中间件（类全限定名称）示例：\\app\\middleware\\Test1；参数（多参数','分割）示例：param1,param2"
            ],
        ];
    }

    // 获取路由请求的公共字段
    public function getRequestCommonField(): array {
        return [
            'desc' => ['title' => '描述', 'verify' => 'require|max:60', 'desc' => '最多支持60位字符'],
            'method' => ['title' => 'method', 'type' => 'select', 'options' => ['' => 'ANY'] + RequestMethod::getDescOptions()],
            'require_params' => [
                'title' => '必要参数',
                'type' => 'table',
                'width' => 500,
                'cols' => [
                    ['title' => '参数key', 'field' => 'key','verify'=>'require'],
                    ['title' => "参数value", 'field' => 'value'],
                ],
            ],
            'enable_login_auth' => EnableLoginAuth::formFieldSwitchArgs() + [
                'title' => '登录鉴权',
                'type' => 'switch',
                'default' => EnableLoginAuth::ON,
                'desc' => '登录鉴权开启时,将校验当前请求用户是否登录'
            ],
        ];
    }

}