<?php

namespace yunj\app\admin\service;

use yunj\app\admin\traits\MemberHelper;
use yunj\app\admin\traits\ModelGet;

abstract class Service {

    use ModelGet, MemberHelper;
}
