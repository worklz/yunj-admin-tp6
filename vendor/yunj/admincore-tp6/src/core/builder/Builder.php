<?php

namespace yunj\core\builder;

use think\Response;
use think\facade\View;
use yunj\core\Validate;
use think\facade\Request;
use yunj\core\response\Json;
use yunj\core\constants\BuilderConst;
use yunj\core\exception\GeneralException;

abstract class Builder {

    /**
     * 类型
     * @var string
     */
    protected $type = "";

    /**
     * 构建器验证器完全限定名称
     * @var string
     */
    protected $builderValidateClass = "";

    /**
     * 需加载的js文件路径列表
     * @var array
     */
    protected $scriptFileList = [];

    /**
     * 创建失败错误信息
     * @var array
     */
    protected $error = [];

    /**
     * 链式操作
     * 注意：按顺序执行
     * @var array
     */
    protected $options = [];

    /**
     * 链式选项回调
     * @var array
     */
    protected $optionsCallable = [];

    /**
     * 系统配置
     * @var array
     */
    protected $config = [];

    /**
     * 必填：ID
     * @var string
     */
    protected $id = "";

    /**
     * 构建器的验证器
     * @var Validate|callable|null
     */
    protected $validate = null;

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getScriptFileList(): array {
        return $this->scriptFileList;
    }

    /**
     * 构造函数
     * @param string $id
     * @param array $args
     */
    public function __construct(string $id = "", array $args = []) {
        $this->id = $id;
        $this->setAttr($args);
    }

    /**
     * Notes: 配置属性参数
     * Author: Uncle-L
     * Date: 2021/10/19
     * Time: 14:11
     * @param array $args
     */
    protected function setAttr(array $args = []): void {
        foreach ($args as $k => $v) {
            if (!in_array($k, $this->options)) continue;
            $this->$k($v);
        }
    }

    /**
     * Notes: 获取构建器验证器实例
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 15:06
     * @return Validate
     */
    protected function getBuilderValidate(): Validate {
        static $validate;
        if ($validate) return $validate;
        $class = $this->builderValidateClass;
        $validate = new $class();
        return $validate;
    }

    /**
     * Notes: 设置错误信息
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 9:57
     * @param string $msg
     * @return $this
     */
    protected function error($msg = '') {
        $this->error[] = $msg;
        return $this;
    }

    /**
     * Notes: 存在错误信息
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:01
     * @return bool
     */
    public function existError() {
        return !!$this->error;
    }

    /**
     * Notes: 获取错误信息
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 9:56
     * @return string
     */
    public function getError() {
        $msg = '';
        foreach ($this->error as $v) {
            $msg .= ",{$v}";
        }
        if ($msg) $msg = substr($msg, 1);
        return $msg;
    }

    /**
     * Notes: 设置链式操作的回调集合
     * Author: Uncle-L
     * Date: 2021/10/8
     * Time: 17:58
     * @param string $option
     * @param callable $callbale
     */
    protected function setOptionCallbale(string $option, callable $callbale) {
        $optionsCallable = $this->optionsCallable && is_array($this->optionsCallable) ? $this->optionsCallable : [];
        $optionsCallable[$option] = isset($optionsCallable[$option]) ? $optionsCallable[$option] : [];
        $optionsCallable[$option][] = $callbale;
        $this->optionsCallable = $optionsCallable;
    }

    /**
     * Notes: 执行链式操作的回调集合
     * Author: Uncle-L
     * Date: 2021/10/8
     * Time: 17:59
     * @param string $want_options [想要执行的指定操作]
     */
    protected function execOptionsCallbale(string $want_options = ''): void {
        $wantOptions = !$want_options ? [] : explode(",", $want_options);
        $optionsCallable = $this->optionsCallable;
        if (!$optionsCallable || !is_array($optionsCallable)) return;
        foreach ($this->options as $option) {
            if (!isset($optionsCallable[$option]) || ($wantOptions && !in_array($option, $wantOptions))) continue;
            $callbales = $optionsCallable[$option];
            foreach ($callbales as $callbale) {
                if ($this->existError()) return;
                $optionMethod = "exec_{$option}_callable";
                $this->$optionMethod($callbale);
            }
            // 防止重复执行
            unset($this->optionsCallable[$option]);
        }
    }

    /**
     * Notes: 渲染输出
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 13:12
     * @return Builder
     * @throws GeneralException
     */
    public function assign() {
        return Request::isAjax() ? $this->async() : $this->view();
    }

    /**
     * Notes: 异步请求
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 14:23
     * @return $this
     */
    public function async() {
        $data = input('post.');
        if (!$data || !is_array($data) || !($builderId = $data[BuilderConst::ID_KEY] ?? null) || $builderId != $this->id) return $this;
        $data['builder'] = $this;
        $validate = $this->getBuilderValidate();
        $res = $validate->scene('AsyncRequest')->check($data);
        if (!$res) {
            return $this;
        }

        $data = $validate->getData();
        $actionName = 'async_' . $data[BuilderConst::ASYNC_TYPE_KEY];
        $response = $this->$actionName($data);
        $response instanceof Json ? throw_json($response) : throw_success_json($response);
        return $this;
    }

    /**
     * Notes: 视图配置渲染
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 12:52
     * @return $this
     * @throws GeneralException
     */
    protected function view() {
        $args = $this->viewArgs();

        $type = $this->type;
        $id = $this->id;

        //构建器模板数据
        $currArgs = [$id => $args];
        $typeArgs = View::__isset($type) ? (View::__get($type) + $currArgs) : $currArgs;
        View::__set($type, $typeArgs);

        // 页面 scriptFileList
        $currScriptFileList = $this->scriptFileList;
        $scriptFileList = View::__isset("scriptFileList") ? array_unique(array_merge(View::__get("scriptFileList"), $currScriptFileList)) : $currScriptFileList;
        View::__set('scriptFileList', $scriptFileList);

        return $this;
    }

    /**
     * Notes: 视图配置
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 14:27
     * @return array
     * @throws GeneralException
     */
    public function viewArgs(): array {
        $this->execOptionsCallbale();
        if ($this->existError()) throw new GeneralException($this->getError());
        $args = [];
        return $args;
    }

    /**
     * Notes: 构建器ID
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:41
     * @param string $id
     * @return Builder
     */
    public function init(string $id) {
        if ($this->existError()) return $this;
        $this->id = $id;
        return $this;
    }


    /**
     * 设置验证器
     * @param callable|string<Validate-class-name> $validate
     * callable:
     *      function(){
     *          // 返回验证器全限定类名
     *          return \\demo\\validate\\TestValidate::class;
     *          // 返回验证器示例
     *          return new TestValidate();
     *      }
     * string:
     *      \\demo\\validate\\TestValidate::class
     * @return static
     */
    public function validate($validate) {
        if ($this->existError()) return $this;
        if (!is_callable($validate)) {
            $callable = function () use ($validate) {
                return $validate;
            };
        } else {
            $callable = $validate;
        }
        $this->validate = $callable;
        return $this;
    }

    /**
     * 验证器常规校验
     * @param array $data 验证规则
     * @param array $data 字段配置
     * @param array $data 待验证数据
     * @param string $scene 验证场景
     * @param bool $autoThrow 自动抛出错误异常
     * @return string   错误信息
     */
    protected function validateCheckByNormal(array $rule, array $field, array &$data, string $scene, bool $autoThrow = true): string {
        if (!$data) return '';
        $validate = $this->getValidate();
        $data['builder'] = $this;
        $res = data_validate($validate, $rule, $field, $data, $this->id . $scene);
        if (!$res && $autoThrow) throw_error_json($validate->getError());
        return $res ? '' : $validate->getError();
    }

    /**
     * 验证器配置字段数据校验
     * @param array $fields
     * @param array $data
     * @param string $scene
     * @param bool $autoThrow 自动抛出错误异常
     * @return string   错误信息
     */
    protected function validateCheckByFieldsData(array $fields, array &$data, string $scene, bool $autoThrow = true): string {
        if (!$fields) return '';
        $validate = $this->getValidate();
        $data['builder'] = $this;
        $res = form_data_validate($validate, $fields, $data, $this->id . $scene);
        if (!$res && $autoThrow) throw_error_json($validate->getError());
        return $res ? '' : $validate->getError();
    }

    /**
     * 获取验证器对象
     * @return Validate
     */
    public function getValidate(): Validate {
        if ($this->validate instanceof Validate) return $this->validate;
        $this->execOptionsCallbale('validate');
        if ($this->existError()) throw_error_json($this->getError());

        $validate = $this->validate;
        if (!$validate) {
            $validate = new Validate();
        } elseif (is_callable($validate)) {
            $validate = call_user_func_array($this->validate, [$this]);
            if (is_string($validate) && class_exists($validate)) $validate = new $validate();
        }
        if (!$validate || !($validate instanceof Validate)) {
            $className = get_class($this);
            $className = substr($className, strripos($className, '\\') + 1);
            throw_error_json($className . " [validate] 需返回 \\yunj\\core\\Validate及其子类 的实例对象或全限定类名");
        }
        $this->validate = $validate;
        return $this->validate;
    }

}