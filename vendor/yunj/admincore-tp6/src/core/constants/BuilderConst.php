<?php

namespace yunj\core\constants;

final class BuilderConst {

    // 唯一标识key
    const ID_KEY = 'builderId';

    // 异步类型key
    const ASYNC_TYPE_KEY = 'builderAsyncType';

    // 异步事件key
    const ASYNC_EVENT_KEY = 'builderAsyncEvent';

}