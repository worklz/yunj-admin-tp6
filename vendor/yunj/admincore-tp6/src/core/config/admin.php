<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 后台常规配置
// +----------------------------------------------------------------------

return [

    /**
     * LayUI版本
     */
    "layui_version" => "2.9.14",

    /**
     * 自定义主题配置
     * 示例：
     *  'theme'=>[
     *      // 主题唯一code
     *      'default'=>[
     *          // 主题标题
     *          'title'=>'默认主题',
     *          // 主题模板样式文件路径
     *          'tpl_style_file'=>'/static/yunj/css/theme/default/tpl.css',
     *          // 主题样式文件路径/static/
     *          'style_file'=>'/static/yunj/css/theme/default/default.css',
     *      ],
     *  ],
     */
    'theme' => [
        'default' => [
            'title' => '默认主题',
            'tpl_style_file' => '/static/yunj/css/theme/default/tpl.min.css',
            'style_file' => '/static/yunj/css/theme/default/default.min.css',
        ],
        'gorgeous' => [
            'title' => '绚丽多彩',
            'tpl_style_file' => '/static/yunj/css/theme/gorgeous/tpl.min.css',
            'style_file' => '/static/yunj/css/theme/gorgeous/gorgeous.min.css',
        ],
    ],

];