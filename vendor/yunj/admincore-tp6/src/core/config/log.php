<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 日志配置
// +----------------------------------------------------------------------

use yunj\app\admin\enum\RedisKey;
use yunj\app\admin\model\AdminRouteRequest;
use yunj\core\constants\BuilderConst;
use yunj\app\admin\enum\TableBuilderEvent;
use yunj\app\admin\service\member\log\LogSaveService;
use yunj\app\admin\service\route\RouteRequestService;

return [

    // 分组
    // 配置/调整完成后需进入项目根目录执行命令：php think yunj:init-system-auths 重置系统权限（或进入权限管理页面执行列表右上角系统权限同步功能）
    'types' => [
        'all' => [
            'title' => '操作日志',
            'icon' => 'layui-icon-list',
            // 表格
            'table' => [
                // 固定查询条件
                'fixed_where' => function (array $filter) {
                    return [];
                },
            ]
        ],
        'login' => [
            'title' => '登录日志',
            'icon' => 'layui-icon-list',
            // 表格
            'table' => [
                // 固定查询条件
                'fixed_where' => function (array $filter) {
                    /** @var RedisKey $loginHandleRequestDataObj */
                    $loginHandleRequestDataObj = RedisKey::ADMIN_LOGIN_HANDLE_ROUTE_REQUEST_DATA();
                    $loginHandleRequestData = $loginHandleRequestDataObj->getCacheValue(86400);
                    /** @var RedisKey $logoutRequestDataObj */
                    $logoutRequestDataObj = RedisKey::ADMIN_LOGOUT_ROUTE_REQUEST_DATA();
                    $logoutRequestData = $logoutRequestDataObj->getCacheValue(86400);
                    $requestIds = [
                        $loginHandleRequestData['id'],
                        $logoutRequestData['id'],
                    ];
                    return [['request_id', 'in', $requestIds]];
                },
            ]
        ]
    ]

];