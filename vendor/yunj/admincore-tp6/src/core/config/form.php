<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 表单配置
// +----------------------------------------------------------------------

return [

    // button的有效值（back返回、clear清空、reload重载、reset重置、search搜索、submit提交）
    'button' => [
        'back' => [
            'desc' => '返回',
            'class' => 'layui-btn-primary',
            'icon' => 'yunj-icon-back',
            'mobileDropDown' => true,
        ],
        'clear' => [
            'desc' => '清空',
            'class' => 'layui-btn-primary',
            'icon' => 'yunj-icon-clear',
            'mobileDropDown' => true,
        ],
        'reset' => [
            'desc' => '重置',
            'class' => 'layui-btn-primary',
            'icon' => 'yunj-icon-refresh',
        ],
        'reload' => [
            'desc' => '重载',
            'class' => 'layui-btn-primary',
            'icon' => 'yunj-icon-refresh',
            'mobileDropDown' => true,
        ],
        'search' => [
            'desc' => '搜索',
            'icon' => 'layui-icon-search',
        ],
        'submit' => [
            'desc' => '提交',
            'icon' => 'yunj-icon-submit',
        ],
    ],

];