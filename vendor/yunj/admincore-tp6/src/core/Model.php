<?php

namespace yunj\core;

use think\Model as ThinkModel;
use yunj\core\traits\ModelHelper;

class Model extends ThinkModel {

    use ModelHelper;

}