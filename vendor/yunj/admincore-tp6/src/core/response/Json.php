<?php

namespace yunj\core\response;

use think\Container;
use think\response\Json as ThinkResponseJson;

class Json extends ThinkResponseJson {

    /**
     * 获取\yunj\core\response\Json对象实例，和json()方法类似
     * @param mixed $data    返回的数据
     * @param int   $code    状态码
     * @param array $header  头部
     * @param array $options 参数
     * @return Json
     */
    public static function generate($data = [], $code = 200, $header = [], $options = []): Json {
        /** @var Json $response */
        $response = Container::getInstance()->invokeClass("\\yunj\\core\\response\\Json", [$data, $code]);
        return $response->header($header)->options($options);
    }

}