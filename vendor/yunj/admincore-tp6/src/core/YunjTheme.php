<?php
namespace yunj\core;

final class YunjTheme{

    // 主题列表
    private $list;

    private function __construct() {
        $this->setAttr();
    }

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function setAttr(){
        $systemThemes=Config::get('admin.theme');
        foreach ($systemThemes as &$v){
            $v['tpl_style_file'].='?v='.YUNJ_VERSION;
            $v['style_file'].='?v='.YUNJ_VERSION;
        }
        $customThemes=yunj_config('admin.theme');
        $this->list=$systemThemes+$customThemes;
    }

    public static function lists($is_json=false){
        return $is_json?json_encode(self::instance()->list):self::instance()->list;
    }

}