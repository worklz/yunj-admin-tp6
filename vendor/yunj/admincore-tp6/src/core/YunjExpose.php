<?php

namespace yunj\core;

use yunj\core\constants\BuilderConst;
use yunj\core\enum\Def;

/**
 * 对外暴露数据
 * Class YunjExpose
 * @package yunj\core
 */
final class YunjExpose {

    public static function config(bool $isJson = false) {
        $data = [
            // 默认值
            'def' => [
                // 表单时间日期字段范围分隔符
                'form_field_date_range' => Def::FORM_FIELD_DATE_RANGE,
                // 默认图片地址
                'image_url' => default_image_url(),
                // 成功提示
                'success_msg' => Def::SUCCESS_MSG,
                // 确认errcode
                'confirm_errcode' => Def::CONFIRM_ERRCODE,
                // 确认弹窗（确认后的提交的已确认参数header key）
                'confirmed_header_key' => Def::CONFIRMED_HEADER_KEY,
            ],
            "admin" => [
                // 后台系统入口
                "entrance" => YUNJ_ADMIN_ENTRANCE,
                // 后台登录地址
                "login_url" => get_controller_action_page_url(yunj_config('admin.login_controller'), 'login'),
            ],
            "file" => [
                // 上传地址
                'upload_url' => build_url('yunjAdminFileUpload'),
                // 上传图片后缀
                'upload_img_ext' => yunj_setting('sys.upload_img_ext'),
                // 上传图片大小（单位MB）
                'upload_img_size' => yunj_setting('sys.upload_img_size'),
                // 上传文件后缀
                'upload_file_ext' => yunj_setting('sys.upload_file_ext'),
                // 上传文件大小（单位MB）
                'upload_file_size' => yunj_setting('sys.upload_file_size'),
                // 上传媒体后缀
                'upload_media_ext' => yunj_setting('sys.upload_media_ext'),
                // 上传媒体大小（单位MB）
                'upload_media_size' => yunj_setting('sys.upload_media_size'),
            ],
            "form" => [
                "button" => Config::get('form.button', [])
            ],
            "builder" => [
                "id_key" => BuilderConst::ID_KEY,
                "async_type_key" => BuilderConst::ASYNC_TYPE_KEY,
                "async_event_key" => BuilderConst::ASYNC_EVENT_KEY,
            ]
        ];
        return $isJson ? json_encode($data, JSON_UNESCAPED_UNICODE) : $data;
    }

}