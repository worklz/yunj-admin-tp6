<?php

namespace yunj\core\control\form;

use yunj\core\Config;
use yunj\core\exception\GeneralException;

final class YunjFormButton {

    const DEFAULT_REQUIRE_ARGS = [
        'desc' => '',                   // 描述
        'class' => '',                  // 按钮class
        'icon' => '',                   // 图标class
        'mobileDropDown' => false,      // 移动端下拉（移动端表单集成到下拉操作里）
        'auth' => '',                   // 权限key
    ];

    /**
     * 校验
     * @param string $key
     * @param string|array $args
     * @return array<error,args>    [错误消息,正常参数]
     */
    public static function check(string $key, $args): array {
        try {
            $configButton = Config::get('form.button', []);
            if (is_string($args)) {
                $args = $configButton[$key] ?? self::DEFAULT_REQUIRE_ARGS;
            }
            if (!is_array($args)) {
                throw new GeneralException('YunjForm [button] 返回值错误');
            }
            $args = array_supp($args, ($configButton[$key] ?? []) + self::DEFAULT_REQUIRE_ARGS);

            // auth 权限校验
            if ($args['auth'] && !admin_member_auth_check($args['auth'])) $args = [];
            return [null, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null];
        }
    }

}