<?php

namespace yunj\core\control\form\field;

class Line extends YunjField {

    protected static function handleArgs(array $args): array {
        $args["readonly"] = true;
        return $args;
    }
    
}