<?php
namespace yunj\core\control\form\field;

class Select extends YunjField {

    protected static function defineExtraArgs():array{
        return [
            'options' => [],         // 选项（例：['man'=>'男','woman'=>'女']）
            'search' => false,      // 开启搜索
        ];
    }

    protected static function handleArgs(array $args): array {
        $optionKeys = array_keys($args["options"]);
        if ($args["default"] === "")
            $args["default"] = current($optionKeys);
        if (!strstr($args["verify"], "in"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "in:" . implode(",", $optionKeys);
        return $args;
    }

}