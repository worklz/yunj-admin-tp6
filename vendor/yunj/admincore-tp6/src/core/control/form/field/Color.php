<?php

namespace yunj\core\control\form\field;

class Color extends YunjField {

    protected static function defineExtraArgs(): array {
        return [];
    }

    protected static function handleArgs(array $args): array {
        if (!strstr($args["verify"], "hexColor"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "hexColor";
        return $args;
    }

}