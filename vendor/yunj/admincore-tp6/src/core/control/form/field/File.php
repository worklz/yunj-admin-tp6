<?php
namespace yunj\core\control\form\field;

class File extends YunjField {

    protected static function defineExtraArgs():array{
        return [
            'multi' => false,                                    // 是否多文件，默认false，单位true是表示多文件上传，默认最多上传9个文件。传入数字时，即表示多文件上传，最多允许上传限制的数字的文件数
            'size'=>yunj_setting('sys.upload_file_size'),   // 限制上传文件大小，默认配置值
            'ext'=>yunj_setting('sys.upload_file_ext'),     // 限制上传文件后缀，默认配置值
            'text'=>"文件上传",                             // 显示文字
        ];
    }

    protected static function handleArgs(array $args): array {
        // verify
        if ($args['multi']) {
            if (!strstr($args["verify"], "urls"))
                $args["verify"] .= ($args["verify"] ? "|" : "") . "urls";
        } else {
            if (!strstr($args["verify"], "url"))
                $args["verify"] .= ($args["verify"] ? "|" : "") . "url";
        }
        return $args;
    }

}