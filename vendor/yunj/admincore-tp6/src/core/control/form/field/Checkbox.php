<?php

namespace yunj\core\control\form\field;

class Checkbox extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'options' => [],           // 选项（例：['read'=>'阅读','write'=>'写作']）
        ];
    }

    protected static function handleArgs(array $args): array {
        if (!strstr($args["verify"], "arrayIn"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "arrayIn:" . implode(",", array_keys($args["options"]));
        return $args;
    }

}