<?php

namespace yunj\core\control\form\field;

use yunj\core\enum\Def;

// 格式yyyy-MM
class YearMonth extends Date {

    protected static function handleArgs(array $args): array {
        // verify
        self::handleArgsVerify($args);
        return $args;
    }

}