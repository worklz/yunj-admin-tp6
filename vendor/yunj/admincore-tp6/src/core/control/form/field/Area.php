<?php

namespace yunj\core\control\form\field;

class Area extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'acc' => 'district',       // 精度（可选值：province、city、district默认）
        ];
    }

    protected static function handleArgs(array $args): array {
        if (!strstr($args["verify"], "area"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "area:" . $args["acc"];
        return $args;
    }

}