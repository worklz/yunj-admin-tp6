<?php
namespace yunj\core\control\form\field;

class DropdownSearch extends YunjField {

    protected static function defineExtraArgs():array{
        return [
            'multi' => true,            // 模式（是否多选，默认多选）
            'options' => [],            // 可选数据数组或可选数据请求地址
            'optionIdKey'=>'id',        // 可选数据唯一标识属性名称。默认为id
            'optionNameKey'=>'name',    // 可选数据展示名称标识字段的属性名称。默认为name
        ];
    }

}