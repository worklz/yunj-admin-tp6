<?php

namespace yunj\core\control\form\field;

use yunj\core\enum\Def;

// 格式yyyy-MM-dd
class Date extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'placeholder' => '',                // 占位符
            'min' => '',                        // 最小日期，格式yyyy-MM-dd
            'max' => '',                        // 最大日期，格式yyyy-MM-dd
            'range' => false,                   // 范围选择开启，默认false
            'shortcuts' => [],                  // 左侧的快捷选择栏
        ];
    }

    protected static function handleArgs(array $args): array {
        // verify
        self::handleArgsVerify($args);
        return $args;
    }

    /**
     * 处理校验规则
     * @param array $args
     */
    protected static function handleArgsVerify(array &$args) {
        $type = $args['type'];
        $verify = $args["verify"];
        if ($args['range'] === false) {
            // 不是范围
            if (!strstr($verify, $type)) $verify .= ($verify ? "|" : "") . $type;
        } else {
            // 范围
            if (!strstr($verify, 'timeRange'))
                $verify .= ($verify ? "|" : "") . ('timeRange:' . $type);
        }
        $args["verify"] = $verify;
    }

}