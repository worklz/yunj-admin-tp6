<?php

namespace yunj\core\control\form\field;

abstract class YunjField {

    const REQUIRE_ARGS = [
        'type' => 'text',               // 类型（默认text）
        'title' => '',                  // 标题（可选）
        'default' => '',                // 默认值（可选，如：['read']）
        'desc' => '',                   // 描述（可选）
        'verify' => '',                 // 验证规则（可选，例：'require|mobile'，必填且为手机格式）
        'verifyTitle' => '',            // 验证标题（可选）
        'readonly' => false,            // 只读（默认false）
        'grid' => [],                   // 栅格布局。超小屏幕(手机<768px)、小屏幕(平板≥768px)、中等屏幕(桌面≥992px)、大型屏幕(桌面≥1200px)、超大屏幕(桌面≥1400px)
        'auth' => '',                   // 权限
    ];

    /**
     * 校验
     * @return array<error,args>    [错误消息,正常参数]
     */
    public static function check(array $args): array {
        try {
            $args = static::setArgs($args);
            // auth 权限校验
            if (isset($args['auth']) && $args['auth'] && !admin_member_auth_check($args['auth'])) $args = [];
            return [null, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null];
        }
    }

    /**
     * @param array $args
     * @return array
     */
    protected static function setArgs(array $args): array {
        $args = array_supp($args, static::defineExtraArgs() + static::REQUIRE_ARGS);
        $args = static::handleArgs($args);
        return $args;
    }

    /**
     * 定义额外参数
     * @return array
     */
    protected static function defineExtraArgs(): array {
        return [];
    }

    /**
     * 处理 args
     * @param array $args
     * @return array
     */
    protected static function handleArgs(array $args): array {
        return $args;
    }

}