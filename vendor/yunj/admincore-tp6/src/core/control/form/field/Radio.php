<?php

namespace yunj\core\control\form\field;

class Radio extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'options' => [],         // 选项（例：['man'=>'男','woman'=>'女']）
        ];
    }

    protected static function handleArgs(array $args): array {
        $optionKeys = array_keys($args["options"]);
        if ($args["default"] === "")
            $args["default"] = $optionKeys[0];
        if (!strstr($args["verify"], "in"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "in:" . implode(",", $optionKeys);
        return $args;
    }

}