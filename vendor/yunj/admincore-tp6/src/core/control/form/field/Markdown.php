<?php

namespace yunj\core\control\form\field;

class Markdown extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'mode' => 'cherry',           // 模式（可选值：cherry（默认）、editormd）
            "modeConfig" => [               // 模式的配置
                "cherry" => [
                    // 完整配置详见：https://github.com/Tencent/cherry-markdown/wiki/%E9%85%8D%E7%BD%AE%E9%A1%B9%E5%85%A8%E8%A7%A3
                    // 顶部工具栏（仅屏幕宽度>768px生效）
                    "toolbar" => [
                        'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'strikethrough', 'color', 'size', '|',
                        'header', 'list', 'hr', 'panel', '|',
                        'image', 'link', 'code', 'table', '|', 'togglePreview',
                    ],
                    // 顶部右侧工具栏（仅屏幕宽度>768px生效）
                    "toolbarRight" => ['export', 'theme', 'copy'],
                    // 移动端顶部工具栏（仅屏幕宽度<=768px生效）
                    "mobileToolbar" => [
                        'undo',
                        ["bold" => ['bold', 'italic', 'underline', 'strikethrough', 'color', 'size']], 'header',
                        ["insert" => ['list', 'hr', 'panel', 'image', 'link', 'code', 'table']], 'togglePreview',
                    ],
                ],
                "editormd" => [
                    "height" => 250,
                    "watch" => false,
                    "placeholder" => "此处开始编写...",
                    "imageFormats" => explode(",", yunj_setting('sys.upload_img_ext')),
                    // 全屏展开编辑有bug
                    "toolbar" => [
                        "undo", "redo", "|", "bold", "del", "italic", "quote", "|"
                        , "h1", "h2", "h3", "h4", "|", "list-ul", "list-ol", "hr", "|"
                        , "align-left", "align-center", "align-right", "align-justify", "|"
                        , "table", "datetime", "html-entities", "pagebreak", "code", "code-block", "|"
                        , "link", "reference-link", "image", "video", "|"
                        , "watch", "preview", /*"fullscreen",*/
                        "clear", "search", "|", "help"
                    ]
                ],
            ],
        ];
    }

    protected static function handleArgs(array $args): array {
        $mode = $args["mode"];
        $modeConfig = $args["modeConfig"];
        $defaultModeConfig = self::defineExtraArgs()["modeConfig"];
        // 没有设置配置
        if (!isset($modeConfig[$mode])) {
            $modeConfig[$mode] = $defaultModeConfig[$mode];
            $args["modeConfig"] = $modeConfig;
            return $args;
        }
        // 有配置
        $modeConfig[$mode] += $defaultModeConfig[$mode];
        if ($mode === "editormd") {
            // 如果是editormd，对toolbar取交集
            $modeConfig[$mode]["toolbar"] = array_intersect($modeConfig[$mode]["toolbar"], $defaultModeConfig[$mode]["toolbar"]);
        }
        $args["modeConfig"] = $modeConfig;
        return $args;
    }

}