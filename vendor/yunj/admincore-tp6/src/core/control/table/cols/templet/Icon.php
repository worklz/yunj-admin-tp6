<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class Icon extends YunjCol {

    protected static function defineExtraArgs(): array {
        return [
            'align' => 'center',
        ];
    }
    
}