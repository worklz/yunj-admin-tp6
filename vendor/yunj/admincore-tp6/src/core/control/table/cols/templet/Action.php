<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class Action extends YunjCol {

    // 操作栏操作项必要配置
    const ACTION_REQUIRE_ARGS = [
        'type' => '',                 // 触发事件类型（openTab 打开子页面、openPopup 打开弹出层页面、asyncEvent 异步事件）
        'title' => '',                // 标题
        'class' => '',                // 额外的class或图标类名
        'url' => '',                  // 触发事件执行的url
        'confirmText' => '',          // 确认弹窗提示
        'dropdown' => false,          // 是否下拉操作
        'show' => true,               // 是否显示（默认为true,bool或string。为字符串时默认会解析为laytpl模板语句，如："d.status===1 && d.age > 10" 表示：当前行数据状态为1且年龄>10才显示此操作项）
        'auth' => '',                 // 操作项对应的权限key
    ];

    protected static function defineExtraArgs(): array {
        return [
            'title' => '操作',
            'align' => 'center',
            'fixed' => 'right',
            'options' => [],
        ];
    }

    protected static function handleArgs(array $args): array {
        $options = $args['options'];
        if (!$options) return $args;
        $args['options'] = [];
        foreach ($options as $k => $v) {
            $option = array_supp($v, self::ACTION_REQUIRE_ARGS);
            // auth 权限判断
            if ($option['auth'] && !admin_member_auth_check($option['auth'])) {
                unset($args['options'][$k]);
                continue;
            }
            $args['options'][$k] = $option;
        }
        return $args['options'] ? $args : [];
    }

}