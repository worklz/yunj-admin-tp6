<?php
namespace yunj\core\control\table\cols\type;

use yunj\core\control\table\cols\YunjCol;

/**
 * Class Data
 * 纯数据，不用于表头展示
 * @package yunj\core\control\table\cols\type
 */
class Data extends YunjCol {
    
}