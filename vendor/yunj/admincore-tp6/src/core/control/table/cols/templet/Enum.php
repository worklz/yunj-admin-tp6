<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class Enum extends YunjCol {

    protected static function defineExtraArgs(): array {
        return [
            'options' => [],     // 枚举元素 key=>value 或 key=>['text'=>'xxx','bgColor'=>'#fff']
        ];
    }

    protected static function handleArgs(array $args): array {
        // options
        $args['options'] = (object)$args['options'];
        return $args;
    }

}