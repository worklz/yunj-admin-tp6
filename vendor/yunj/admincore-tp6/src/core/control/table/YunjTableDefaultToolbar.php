<?php

namespace yunj\core\control\table;

use yunj\core\Config;
use yunj\core\exception\GeneralException;

final class YunjTableDefaultToolbar {

    const DEFAULT_REQUIRE_ARGS = [
        'type' => '',                 // 触发事件类型（openTab 打开子页面、openPopup 打开弹出层页面、asyncEvent 异步事件）
        'title' => '',                // 标题
        'class' => '',                // 图标类名或额外的class
        'url' => '',                  // 触发事件执行的url
        'confirmText' => '',          // 确认弹窗提示（可选）
        'auth' => '',                 // 权限key
    ];

    /**
     * 校验
     * @param string|mixed $key
     * @return array<error,key,args>    [错误消息,键名,正常参数]
     */
    public static function check($key, $args): array {
        try {
            if (is_string($args)) {
                [$key, $args] = self::checkString($key, $args);
            } elseif (is_array($args)) {
                [$key, $args] = self::checkArray($key, $args);
            } else {
                throw new GeneralException('YunjTable [defaultToolbar] 返回值错误');
            }
            // auth 权限校验
            if ($args['auth'] && !admin_member_auth_check($args['auth'])) $args = [];
            return [null, $key, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null, null];
        }
    }

    /**
     * 校验字符串类型参数
     * @param string|mixed $key
     * @param string $args
     * @return array<key,args>    [键名,正常参数]
     * @throws GeneralException
     */
    private static function checkString($key, string $args): array {
        $configDefaultToolbar = Config::get('table.defaultToolbar', []);
        if (in_array($args, $configDefaultToolbar)) {
            return [$args, self::DEFAULT_REQUIRE_ARGS];
        }
        if ($key == 'import') {
            $args = self::importArgsSupp(['url' => $args] + self::DEFAULT_REQUIRE_ARGS);
            return [$key, $args];
        }
        throw new GeneralException("YunjTable [defaultToolbar] 返回值不在有效范围=>" . json_encode($configDefaultToolbar));
    }

    /**
     * 校验数组类型参数
     * @param string|mixed $key
     * @param array $args
     * @return array<key,args>    [键名,正常参数]
     * @throws GeneralException
     */
    private static function checkArray($key, array $args): array {
        $args = array_supp($args, self::DEFAULT_REQUIRE_ARGS);
        if (in_array($args['type'], ['openTab', 'openPopup'])) {
            if (!$args['url']) throw new GeneralException("YunjTable [defaultToolbar] 自定义配置 [type]='openTab'或'openPopup'时，[url] 不能为空");
        }
        if ($key == 'import') {
            $args = self::importArgsSupp($args);
            return [$key, $args];
        }
        return [$key, $args];
    }

    /**
     * 导入参数补充
     * @param array $args
     * @return array
     */
    private static function importArgsSupp(array $args): array {
        if (!($args['type'] ?? null)) {
            $args['type'] = 'openPopup';
        }
        if (!($args['title'] ?? null)) {
            $args['title'] = '数据导入';
        }
        if (!($args['class'] ?? null)) {
            $args['class'] = 'layui-icon-upload';
        }
        return $args;
    }

}