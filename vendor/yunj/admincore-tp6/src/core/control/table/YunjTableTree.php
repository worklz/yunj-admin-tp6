<?php

namespace yunj\core\control\table;

use yunj\core\Config;
use yunj\core\exception\GeneralException;

final class YunjTableTree {

    const DEFAULT_ARGS = [
        "nameField" => "name",    //「节点索引」属性名。默认为name
    ];

    /**
     * 校验
     * @param string|mixed $args
     * @return array<error,args>    [错误消息,正常参数]
     */
    public static function check($args): array {
        try {
            if (is_string($args)) {
                if (!$args) {
                    throw new GeneralException("YunjTable [tree] 不能为空");
                }
                $args = [
                    "nameField" => $args
                ];
            }
            if ($args === false) {
                return [null, []];
            }
            if ($args === true) {
                $args = [null, self::DEFAULT_ARGS];
            }
            if (!is_array($args)) {
                throw new GeneralException("YunjTable [tree] 需返回有效数组");
            }
            $args += self::DEFAULT_ARGS;
            return [null, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null];
        }
    }

}