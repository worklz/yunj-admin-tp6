<?php

namespace yunj\library\traits;

trait Redis {

    /**
     * @var \Redis
     */
    protected $redis;

    /**
     * 获取redis连接回调
     * @var callable
     */
    protected $redisConnCall;

    /**
     * redis获取（断线重连）
     * 获取redis实例时，如果ping不通或者出现异常，就重新连接
     * ping()会触发Notice错误，Notice: Redis::ping(): send of 14 bytes failed with errno=10054
     * 因为try catch  捕捉不到notice异常，所以ping不通直接重新连接，catch捕捉新连接的实例没有连接上，下次执行ping触发
     * @return \Redis
     */
    public function getRedis(): \Redis {
        if (!$this->redis) {
            $this->setRedis();
            return $this->redis;
        }
        // 判断redis连接是否断开，断开则重新连接
        try {
            // 调用ping之前先抛出个notice异常
            @trigger_error('flag', E_USER_NOTICE);
            $this->redis->ping();
            // 用error_get_last获取最后一个错误，如果错误信息跟我们抛出的一样，说明ping通了，否则抛出个异常 ，让catch捕捉到执行重连
            $error = error_get_last();
            if ($error['message'] != 'flag') throw new \RuntimeException('Redis server error!'.json_encode($error,JSON_UNESCAPED_UNICODE));
        } catch (\Throwable $e) {
            $this->setRedis();
        }
        return $this->redis;
    }

    /**
     * @param \Redis|null $redis
     * @return static
     */
    protected function setRedis() {
        $redisConnCall = $this->redisConnCall?:function(){
            $redis = new \Redis();
            $redis->connect("127.0.0.1", 6379);
            return $redis;
        };
        $this->redis = $redisConnCall();
        // 判断连接是否成功，不成功会抛异常
        $this->redis->ping();
        return $this;
    }

    /*
     * 设置获取redis连接的回调方法
     * @param callable $call
     * @return static
     */
    public function setRedisConnCall(callable $call){
        $this->redisConnCall = $call;
        return $this;
    }
}