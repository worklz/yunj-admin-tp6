# Redis锁

#### 使用示例

```php

function lock(string $key, int $ttl = 10, int $waitTime = 60): Lock {
    $lock = new Lock($key,$ttl,$waitTime);
    // 如果redis实例需要执行其他操作，可调用setRedisConnCall()方法，设置redis连接回调函数
    $lock->setRedisConnCall(function () {
        $redis = ...;
        return $redis;
    });
    // 如果不能运行lua，可调用setLua()方法，表示lua脚本不可用
    $lock->setLua(false);
    return $lock->get();
}

try {
    // 获取锁
    $lock = lock("...key...", 10);
    DB::transaction(function () use (..., $lock) {
        // ...业务处理
        $lock->timeoutThrow();  // 判断执行是否超时
        // ...业务处理
        $lock->timeoutThrow();  // 判断执行是否超时
        // ...业务处理
        $lock->timeoutThrow();  // 判断执行是否超时
    });
} catch (\Throwable $e) {
    // 可记录日志
}
// 释放锁
isset($lock) && $lock->release();

var_dump(nextId());
```