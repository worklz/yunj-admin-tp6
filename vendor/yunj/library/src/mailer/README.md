# 邮件推送

使用swiftmailer发送邮件

#### 简单文字推送

```php
use yunj\library\mailer\Mailer;

$mailer = new Mailer([
    'mailer' => 'smtp',         // 使用服务
    'host' => 'smtp.163.com',   // 发送服务的SMTP服务器地址
    'port' => 465,              // 发送服务的SMTP服务器端口
    'encryption' => 'ssl',      // 发送服务的SMTP加密模式
    'username' => 'xxxx@163.com',           // 发送服务的邮箱
    'password' => 'xxxxxxxxxxxx',           // 发送服务的邮箱授权密码
    'from_address' => 'xxxx@163.com',       // 发件人邮箱
    'from_name' => 'XXXX',                  // 发件人名称
]);
$body = '你好'.date('Y-m-d H:i:s');
$res1 = $mailer->send('收件人邮箱@qq.com', '测试标题'.date('H:i:s'), $body); // 单条邮件推送   返回成功收件人的数量。为0，表示失败
var_dump($res1);
$res2 = $mailer->send(['收件人邮箱1@qq.com','收件人邮箱2@qq.com'], '测试标题'.date('H:i:s'), $body);  // 多条邮件推送
var_dump($res2);
```

#### Html结构推送

```php
use yunj\library\mailer\Mailer;

...

$body = <<<INFO
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>测试123</title>
</head>
<body style="margin: 0;padding: 0;">
<div class="container" style="padding: 30px 0;font-size: 14px;color: #8a8a8a;">
    <div class="content" style="border: 2px solid #eee;border-left: 0;border-right: 0;">
        <p class="bold" style="font-size: 16px;font-weight: 700;color: #393D49;">嗨，123！您好：</p>
        <p class="bold" style="font-size: 16px;font-weight: 700;color: #393D49;">您正在进行<span class="red" style="color: #FF5722;">测试</span>操作，您的验证码为：<span class="red" style="color: #FF5722;">123456</span></p>
        <p>注意：工作人员不会向您索取此验证码，请勿泄露！</p>
    </div>
    <div class="footer">
        <p>此为系统邮件，请勿回复</p>
        <p>请保管好您的邮箱，避免账号被他人盗用</p>
        <br>
        <p style="text-align: right">来自云静定位</p>
    </div>
</div>
</body>
</html>
INFO;
$res = $mailer->send('收件人邮箱@qq.com', '测试标题'.date('H:i:s'), $body);
var_dump($res);
```

#### 附件推送

```php
use yunj\library\mailer\Mailer;

...

$body = '你好'.date('Y-m-d H:i:s');
// attach(文件地址, 文件名称) 文件名称可不设置，默认取地址最后一个"/"后的内容为文件名
$res = $mailer->attach('http://www.xxx.com/123.png')->send('收件人邮箱@qq.com', '测试标题'.date('H:i:s'), $body); 
var_dump($res);

```