# 雪花算法

#### 使用示例

```php

function nextId(){
    $snowflake = new \yunj\library\snowflake\Snowflake();
    // 如果redis实例需要执行其他操作，可调用setRedisConnCall()方法，设置redis连接回调函数
    $snowflake->setRedisConnCall(function () {
        $redis = ...;
        return $redis;
    });
    // 如果不能运行lua，可调用setLua()方法，表示lua脚本不可用
    $snowflake->setLua(false);
    return $snowflake->nextId();
}

var_dump(nextId());
```