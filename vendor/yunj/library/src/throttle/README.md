# 限流

#### 滑动窗口限流示例

```php
use yunj\library\throttle\SlidingWindowThrottle;

// 生成当前请求标识（可以自定义）
$key = 'yunj.admin.tp6.throttle:' . SlidingWindowThrottle::getRequestKey();
$throttle = new SlidingWindowThrottle('20/m',$key);
// 如果redis实例需要执行其他操作，可调用setRedisConnCall()方法，设置redis连接回调函数
$throttle->setRedisConnCall(function () {
    $redis = ...;
    return $redis;
});
// 判断当前请求是否通过
if($throttle->check()){
    var_dump('请求通过');
}else{
    var_dump('请求过于频繁，请在'.$throttle->getWaitSeconds().'秒后重试');
}
```