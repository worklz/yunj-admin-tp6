# 云静资源

[![](https://img.shields.io/badge/Author-Uncle.L-orange.svg)](https://gitee.com/worklz/yunj-library)
[![star](https://gitee.com/worklz/yunj-library/badge/star.svg?theme=dark)](https://gitee.com/worklz/yunj-library/stargazers)
[![fork](https://gitee.com/worklz/yunj-library/badge/fork.svg?theme=dark)](https://gitee.com/worklz/yunj-library)

## 简介

**云静资源**提供常用的帮助类和公共方法

## 安装更新

注意：最近phpcomposer镜像存在问题，可以改成：`composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/`
   
> 安装

```
composer require yunj/library
```
   
> 更新

```
composer update yunj/library
```

## QQ交流群

* `692392147`

## 参与开发

直接提交PR或者Issue即可