# 云静Admin TP6

[![](https://img.shields.io/badge/Author-Uncle.L-orange.svg)](https://gitee.com/worklz/yunj-admin-tp6)
[![](https://img.shields.io/badge/version-v4.4.52-brightgreen.svg)](https://gitee.com/worklz/yunj-admin-tp6)
[![star](https://gitee.com/worklz/yunj-admin-tp6/badge/star.svg?theme=dark)](https://gitee.com/worklz/yunj-admin-tp6/stargazers)
[![fork](https://gitee.com/worklz/yunj-admin-tp6/badge/fork.svg?theme=dark)](https://gitee.com/worklz/yunj-admin-tp6)

## 简介

**云静Admin TP6**是一款免费开源且自适应移动/PC的PHP后台开发框架，基于ThinkPHP6 + LayUI集成了表单/表格构建器等相关功能模块，以方便开发者快速构建自己的应用。在使用过程中，若发现待优化问题请及时与我们取得联系。
<p align="right" >—— Uncle.L</p>

## 文档

安装、开发、更新详见：[云静Admin TP6](https://tp6admin.doc.iyunj.cn)

## 演示

PC端截图 | 移动端截图
--- | ---
![](http://tp6admin.doc.iyunj.cn/assets/imgs/admin-pc-home.png) | ![](http://tp6admin.doc.iyunj.cn/assets/imgs/admin-mobile-home.png)

* 演示地址：[http://tp6admin.iyunj.cn/admin](http://tp6admin.iyunj.cn/admin)

## QQ交流群

* `692392147`

## 参与开发

直接提交PR或者Issue即可